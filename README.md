# Subbed

## Getting Started

### Requirements

You'll need the following things before you can get started:

- Elixir v1.10
- PostgreSQL (Check out [PostgresApp](https://postgresapp.com) if you're on MacOS)
- ImageMagick (for creating thumbnails)

## Running

To start your Phoenix server:

- Copy `config/dev.exs.example` to `config/dev.exs`
- Install dependencies with `mix deps.get`
- Create and migrate your database with `mix ecto.setup`
- Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

### OAuth

You'll need to get the `GOOGLE_CLIENT_ID` and `GOOGLE_CLIENT_SECRET` from someone before you can login to the application.

## Contexts

### Models

When defining a model for a context, you'll want to use the `Subbed.ModelSchema` module. We define this module because timestamps are stored as `:timestamptz` which are timestamps with time zone (in UTC). This is because we will return things based upon their insertion date to the user and we want to make sure that we appropriately can convert those datetimes into times in the user's timezone.

This just means, when defining a model that uses `timestamps()` make sure that you include: `use Subbed.ModelSchema`.

Quick example:
```elixir
defmodule Subbed.Account.User do
  use Subbed.ModelSchema
  alias Subbed.{Content, Schedule}
  # Your code goes here
end
```

I see the the following contexts:

- Schedules

  - Creators
    - id
    - name
    - default platform
    - timezone
  - Events
    - id
    - start time
    - end time
    - description
    - platform
  - Platforms
    - id
    - name
    - url?

- Accounts
  - Users (basic info like id, email, etc)
  - User Profile
  - Authorized Users
    - creator_id
    - user_id
