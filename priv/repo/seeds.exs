# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Subbed.Repo.insert!(%Subbed.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

Subbed.Seeders.Users.seed()
Subbed.Seeders.Platforms.seed()
Subbed.Seeders.Creators.seed()
