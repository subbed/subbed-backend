defmodule Subbed.Repo.Migrations.AddCreatorMaintainers do
  use Ecto.Migration

  def change do
    create table(:maintainers) do
      add(
        :creator_id,
        references(:creators, on_delete: :delete_all, on_update: :update_all, null: false)
      )

      add(
        :user_id,
        references(:users, on_delete: :delete_all, on_update: :update_all, null: false)
      )

      timestamps()
    end

    create(unique_index(:maintainers, [:user_id, :creator_id]))

    # Just in case, we don't really need to worry about this since we're not deployed yet
    execute("""
    INSERT INTO maintainers(creator_id, user_id) SELECT id, owner_id FROM creators;
    """)

    alter table(:creators) do
      remove(:owner_id)
      remove(:default_platform)
    end
  end
end
