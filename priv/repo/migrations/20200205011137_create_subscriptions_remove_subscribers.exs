defmodule Subbed.Repo.Migrations.CreateSubscriptionsRemoveSubscribers do
  use Ecto.Migration
  alias Subbed.Enums

  def down do
    drop table("subscriptions")
    Enums.SubscriptionTypeEnum.drop_type()

    create table(:subscribers) do
      add :user_id,
          references(:users, on_delete: :delete_all, on_update: :update_all, null: false),
          primary_key: false

      add :creator_id,
          references(:creators, on_delete: :delete_all, on_update: :update_all, null: false),
          primary_key: false

      timestamps()
    end

    create index(:subscribers, [:user_id])
    create index(:subscribers, [:creator_id])

    create(
      unique_index(:subscribers, [:user_id, :creator_id], name: :user_id_creator_id_unique_index)
    )
  end

  def up do
    drop table(:subscribers)
    Enums.SubscriptionTypeEnum.create_type()

    create table(:subscriptions) do
      add :user_id,
          references(:users, on_delete: :restrict, on_update: :update_all, null: false),
          primary_key: false

      add :creator_id,
          references(:creators, on_delete: :restrict, on_update: :update_all, null: false),
          primary_key: false

      add :medium_id,
          references(:media, on_delete: :delete_all, on_update: :update_all, null: true),
          primary_key: false

      add :type, Enums.SubscriptionTypeEnum.type(), null: true

      timestamps()
    end

    create index(:subscriptions, [:user_id])
    create index(:subscriptions, [:creator_id])
    create index(:subscriptions, [:medium_id])

    create(
      unique_index(:subscriptions, [:user_id, :medium_id], name: :user_id_medium_id_unique_index)
    )
  end
end
