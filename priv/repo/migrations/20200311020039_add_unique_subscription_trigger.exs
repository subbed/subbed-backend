defmodule Subbed.Repo.Migrations.AddUniqueSubscriptionTrigger do
  use Ecto.Migration

  def up do
    create_function_query = """
    CREATE FUNCTION validate_unique_subscription()
    RETURNS trigger AS $$
    DECLARE has_existing_subscription BOOLEAN;
      BEGIN
        IF NEW.creator_id IS NOT NULL THEN
          SELECT EXISTS(
            SELECT id
            FROM subscriptions
            WHERE creator_id = NEW.creator_id
            AND user_id = NEW.user_id
            AND medium_id IS NULL
          ) INTO has_existing_subscription;

          IF has_existing_subscription THEN
            RAISE 'User (id: %) is already subscribed to Creator with ID %', NEW.user_id, NEW.creator_id
            USING ERRCODE = 'integrity_constraint_violation';
          END IF;

        END IF;
        RETURN NEW;
      END;
      $$ LANGUAGE plpgsql;
    """

    create_trigger_query = """
    CREATE TRIGGER unique_subscription
    BEFORE INSERT ON subscriptions
    FOR EACH ROW
    EXECUTE FUNCTION validate_unique_subscription();
    """

    execute(create_function_query)
    execute(create_trigger_query)
  end

  def down do
    execute("DROP TRIGGER IF EXISTS unique_subscription ON subscriptions;")
    execute("DROP FUNCTION IF EXISTS validate_unique_subscription;")
  end
end
