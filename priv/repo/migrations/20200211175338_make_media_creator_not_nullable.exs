defmodule Subbed.Repo.Migrations.MakeMediaCreatorNotNullable do
  use Ecto.Migration

  def change do
    alter table(:media) do
      modify(:creator_id, :bigint, null: false)
    end
  end
end
