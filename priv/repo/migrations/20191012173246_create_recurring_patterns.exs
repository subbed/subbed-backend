defmodule Subbed.Repo.Migrations.CreateRecurringPatterns do
  use Ecto.Migration
  alias Subbed.Enums

  def change do
    # Based off of this article:
    # https://www.vertabelo.com/blog/again-and-again-managing-recurring-events-in-a-data-model/
    alter table(:events) do
      add(:start_date, :date, null: false)

      add(:end_date, :date,
        comment: "A null end_date means that this event never stops recurring."
      )

      modify(:start_time, :timetz, from: :timestamptz)
      modify(:end_time, :timetz, from: :timestamptz)

      add(:is_full_day_event, :boolean, null: false, default: false)
      add(:is_recurring, :boolean, null: false, default: false)

      add(:parent_event_id, references(:events, on_delete: :nilify_all),
        comment:
          "In the case that a recurrence pattern changes, we still have a reference to the original event."
      )
    end

    Enums.RecurringTypeEnum.create_type()

    create table(:recurring_patterns) do
      add(:recurring_type, Enums.RecurringTypeEnum.type(), null: false)

      add(:separation_count, :integer,
        default: 0,
        null: false,
        comment:
          "The interval in recurring_type before the next event instance is allowed. Ex: if an event occurs every other week, this value is 1."
      )

      add(:day_of_week, :integer,
        comment: "A value between 0 and 6 to match Postgres' DOW function."
      )

      add(:week_of_month, :integer)
      add(:day_of_month, :integer)
      add(:month_of_year, :integer)
      add(:event_id, references(:events, on_delete: :nothing))
    end

    create(
      constraint(:recurring_patterns, :must_be_within_range,
        check: "day_of_week >= 0 and day_of_week < 7"
      )
    )

    create(index(:recurring_patterns, [:event_id]))
    create(index(:events, [:parent_event_id]))
  end
end
