defmodule Subbed.Repo.Migrations.AddMediaCreatorAssociation do
  use Ecto.Migration

  def change do
    alter table(:media) do
      add :creator_id,
          references(:creators, on_delete: :delete_all, on_update: :update_all, null: false),
          primary_key: false
    end

    create index(:media, [:creator_id])
    create(unique_index(:media, [:name, :creator_id]))
  end
end
