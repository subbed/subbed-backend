defmodule Subbed.Repo.Migrations.CreateEvents do
  use Ecto.Migration

  def change do
    create table(:events) do
      add(:streamer_id, references(:streamers, on_delete: :restrict, on_update: :update_all),
        null: false
      )

      add(
        :creator_id,
        references(:users,
          on_delete: :restrict,
          on_update: :restrict
        ),
        null: false
      )

      add(:name, :string, null: false, default: "")
      add(:description, :string, null: false, default: "")
      add(:start_time, :timestamptz, null: false)
      add(:end_time, :timestamptz, null: false)

      add(
        :platform,
        references(:platforms,
          column: :name,
          type: :string,
          on_delete: :nothing,
          on_update: :update_all
        ),
        null: false
      )

      timestamps()
    end

    create(index(:events, [:streamer_id]))
    create(index(:events, [:creator_id]))
    create(index(:events, [:platform]))
  end
end
