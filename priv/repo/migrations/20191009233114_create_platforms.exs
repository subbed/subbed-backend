defmodule Subbed.Repo.Migrations.CreatePlatforms do
  use Ecto.Migration

  def change do
    create table(:platforms) do
      add(:name, :string, null: false)
      add(:url, :string, null: false)

      timestamps()
    end

    create(unique_index(:platforms, [:name]))
  end
end
