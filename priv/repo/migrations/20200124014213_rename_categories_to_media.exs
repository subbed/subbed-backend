defmodule Subbed.Repo.Migrations.RenameCategoriesToMedia do
  use Ecto.Migration
  alias Subbed.Enums

  def up do
    drop constraint(:events, "events_category_id_fkey")
    drop constraint(:categories, "categories_pkey")
    drop index(:categories, [:name])
    rename table(:categories), to: table(:media)

    Enums.MediaTypeEnum.create_type()

    alter table(:media) do
      modify :id, :bigint, primary_key: true
      add(:type, Enums.MediaTypeEnum.type(), null: false)
    end

    rename table("events"), :category_id, to: :medium_id

    alter table(:events) do
      modify :medium_id, references(:media), null: false
    end

    create index(:media, [:name])
    create index(:media, [:type])
  end

  def down do
    drop constraint(:events, "events_media_id_fkey")
    drop constraint(:media, "media_pkey")
    drop index(:media, [:type])
    drop index(:media, [:name])
    rename table(:media), to: table(:categories)

    alter table(:categories) do
      modify :id, :bigint, primary_key: true
      remove :type
    end

    rename table("events"), :medium_id, to: :category_id

    alter table(:events) do
      modify :category_id, references(:categories)
    end

    create index(:categories, [:name])
  end
end
