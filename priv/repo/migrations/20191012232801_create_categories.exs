defmodule Subbed.Repo.Migrations.CreateCategories do
  use Ecto.Migration

  def change do
    create table(:categories) do
      add(:name, :string, null: false)

      timestamps()
    end

    alter table(:events) do
      add(
        :category_id,
        references(:categories, on_delete: :restrict, on_update: :update_all, null: false)
      )
    end

    create(unique_index(:categories, [:name]))
  end
end
