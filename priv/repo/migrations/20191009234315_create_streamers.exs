defmodule Subbed.Repo.Migrations.CreateStreamers do
  use Ecto.Migration

  def change do
    # This idea is stolen from here: https://stackoverflow.com/a/28909467

    # Postgres doesn't have a timezone data type, but we want to make sure that
    # we don't get weird timezones in the database. To do that, we'll
    # create a timezone table from all the valid timezones in Postgres

    # Create a timezone table
    create table(:timezones) do
      add(:name, :string, null: false)
    end

    create(unique_index(:timezones, [:name]))

    # Populate it with valid PG timezones
    execute("INSERT INTO timezones(name) SELECT name FROM pg_timezone_names;")

    create table(:streamers) do
      add(:name, :string, null: false)

      add(
        :timezone,
        references(:timezones,
          column: :name,
          type: :string,
          on_delete: :restrict,
          on_update: :restrict
        ),
        null: false
      )

      add(
        :default_platform,
        references(:platforms,
          column: :name,
          type: :string,
          on_delete: :restrict,
          on_update: :restrict
        ),
        null: false
      )

      add(
        :creator_id,
        references(:users,
          on_delete: :restrict,
          on_update: :restrict
        ),
        null: false
      )

      timestamps()
    end

    create(unique_index(:streamers, [:name]))
    create(index(:streamers, [:default_platform]))
  end
end
