defmodule Subbed.Repo.Migrations.MakeSubscriptionColumnsNotNullable do
  use Ecto.Migration

  def change do
    alter table(:subscriptions) do
      modify(:creator_id, :bigint, null: false)
      modify(:user_id, :bigint, null: false)
    end
  end
end
