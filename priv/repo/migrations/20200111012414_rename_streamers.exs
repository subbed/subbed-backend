defmodule Subbed.Repo.Migrations.RenameStreamers do
  use Ecto.Migration

  def change do
    drop index("streamers", [:name])
    drop index("streamers", [:default_platform])
    rename table("streamers"), to: table("creators")
    rename table("creators"), :creator_id, to: :owner_id
    create(unique_index(:creators, [:name]))
    create(index(:creators, [:default_platform]))

    drop index("events", [:streamer_id])
    drop index("events", [:creator_id])

    rename table("events"), :creator_id, to: :owner_id
    rename table("events"), :streamer_id, to: :creator_id

    create(index(:events, [:creator_id]))
    create(index(:events, [:owner_id]))

    drop index("subscribers", [:user_id, :streamer_id], name: :user_id_streamer_id_unique_index)
    rename table("subscribers"), :streamer_id, to: :creator_id

    create(
      unique_index(:subscribers, [:user_id, :creator_id], name: :user_id_creator_id_unique_index)
    )
  end
end
