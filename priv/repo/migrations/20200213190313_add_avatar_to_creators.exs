defmodule Subbed.Repo.Migrations.AddAvatarToCreators do
  use Ecto.Migration

  def change do
    alter table(:creators) do
      add :avatar, :text
    end

    create constraint(:creators, :max_avatar_length, check: "length(avatar) <= 500")
  end
end
