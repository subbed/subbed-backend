defmodule Subbed.Repo.Migrations.FixCreatorIndexName do
  use Ecto.Migration

  def change do
    drop constraint(:creators, "streamers_timezone_fkey")
    drop constraint(:events, "events_streamer_id_fkey")
    drop constraint(:subscribers, "subscribers_streamer_id_fkey")
    drop index(:subscribers, [:streamer_id])
    create index(:subscribers, [:creator_id])
    drop constraint(:maintainers, "maintainers_creator_id_fkey")
    drop constraint(:creators, "streamers_pkey")

    alter table(:creators) do
      modify :id, :bigint, primary_key: true

      modify :timezone,
             references(:timezones,
               column: :name,
               type: :string,
               on_delete: :restrict,
               on_update: :restrict
             ),
             null: false
    end

    alter table(:subscribers) do
      modify :creator_id,
             references(:creators, on_delete: :delete_all, on_update: :update_all, null: false)
    end

    alter table(:maintainers) do
      modify :creator_id, references(:creators)
    end
  end
end
