defmodule Subbed.Repo.Migrations.CreateSubscribers do
  use Ecto.Migration

  def change do
    create table(:subscribers) do
      add :user_id,
          references(:users, on_delete: :delete_all, on_update: :update_all, null: false),
          primary_key: false

      add :streamer_id,
          references(:streamers, on_delete: :delete_all, on_update: :update_all, null: false),
          primary_key: false

      timestamps()
    end

    create index(:subscribers, [:user_id])
    create index(:subscribers, [:streamer_id])

    create(
      unique_index(:subscribers, [:user_id, :streamer_id], name: :user_id_streamer_id_unique_index)
    )
  end
end
