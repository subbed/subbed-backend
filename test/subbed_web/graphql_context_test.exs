defmodule SubbedWeb.GraphqlContextTest do
  use SubbedWeb.ConnCase
  alias SubbedWeb.GraphqlContext

  import Plug.Test

  setup do
    conn = init_test_session(build_conn(), %{})
    %{conn: conn}
  end

  describe "add_user" do
    setup context do
      user = insert(:user)
      Map.put(context, :user, user)
    end

    test "adds the user to the context if they're found in the connection's session", context do
      %{conn: conn, user: user} = context
      conn = put_session(conn, :user_id, user.id)
      actual_graphql_context = GraphqlContext.add_user(%GraphqlContext{}, conn)
      expected_graphql_context = %GraphqlContext{user: user}
      assert actual_graphql_context == expected_graphql_context
    end

    test "is a no-op if no user is in the session", %{conn: conn} do
      actual_graphql_context = GraphqlContext.add_user(%GraphqlContext{}, conn)
      assert actual_graphql_context == %GraphqlContext{}
    end
  end
end
