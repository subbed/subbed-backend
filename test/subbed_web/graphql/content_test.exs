defmodule SubbedWeb.Graphql.ContentTest do
  use SubbedWeb.ConnCase

  describe "mutation add_maintainer" do
    setup do
      creator = insert(:creator)
      user = insert(:user)
      conn = build_conn()

      mutation = """
      mutation addMaintainer($userId: Int!, $creatorId: Int!) {
        addMaintainer(creatorId: $creatorId, userId: $userId) {
          id
        }
      }
      """

      variables = %{
        "userId" => user.id,
        "creatorId" => creator.id
      }

      %{creator: creator, user: user, conn: conn, mutation: mutation, variables: variables}
    end

    test "successfully allows you to add a new maintainer", context do
      %{user: user, conn: conn, mutation: mutation, variables: variables} = context

      response =
        conn
        |> post("/api", %{query: mutation, variables: variables})
        |> json_response(200)

      refute response["errors"]
      actual_user_id = get_in(response, ["data", "addMaintainer", "id"])
      assert to_string(user.id) == actual_user_id
    end

    test "returns an error for an invalid user_id", context do
      %{conn: conn, mutation: mutation, variables: variables} = context

      response =
        conn
        |> post("/api", %{query: mutation, variables: Map.put(variables, "userId", 0)})
        |> json_response(200)

      expected_error = %{
        "locations" => [%{"column" => 0, "line" => 2}],
        "message" => "user_id does not exist",
        "path" => ["addMaintainer"]
      }

      assert response["errors"] == [expected_error]
      refute response["data"]["addMaintainer"]
    end

    test "returns an error for an invalid creator_id", context do
      %{conn: conn, mutation: mutation, variables: variables} = context

      response =
        conn
        |> post("/api", %{query: mutation, variables: Map.put(variables, "creatorId", 0)})
        |> json_response(200)

      expected_error = %{
        "locations" => [%{"column" => 0, "line" => 2}],
        "message" => "creator_id does not exist",
        "path" => ["addMaintainer"]
      }

      assert response["errors"] == [expected_error]
      refute response["data"]["addMaintainer"]
    end
  end
end
