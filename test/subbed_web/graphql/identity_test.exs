defmodule SubbedWeb.Graphql.IdentityTest do
  use SubbedWeb.ConnCase

  import Plug.Test

  describe "identity" do
    setup do
      query = """
      query {
        identity {
          id
          userId
        }
      }
      """

      user = insert(:user)

      logged_in_conn =
        build_conn()
        |> init_test_session(%{user_id: user.id})

      conn = build_conn() |> init_test_session(%{})

      %{query: query, user: user, conn: conn, logged_in_conn: logged_in_conn}
    end

    test "returns the user found in the session", context do
      %{
        query: query,
        user: user,
        logged_in_conn: conn
      } = context

      response =
        conn
        |> post("/api", %{query: query})
        |> json_response(200)

      %{"identity" => actual_user} = response["data"]
      assert actual_user["id"] == to_string(user.id)
      assert actual_user["userId"] == user.user_id
    end

    test "returns null if the user isn't found in the session", context do
      %{
        query: query,
        conn: conn
      } = context

      user =
        conn
        |> post("/api", %{query: query})
        |> json_response(200)
        |> get_in(["data", "identity"])

      refute user["userId"]
    end
  end
end
