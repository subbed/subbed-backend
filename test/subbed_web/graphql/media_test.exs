defmodule SubbedWeb.Graphql.MediumTest do
  use SubbedWeb.ConnCase
  alias Subbed.{Content, Schedule, Repo}
  import Ecto.Query

  describe "list_media" do
    setup do
      query = """
      query {
        media {
          id
          name
          type
          creatorId
          subscriptionCount
        }
      }
      """

      creator = insert(:creator)
      user = insert(:user)
      medium = insert(:medium, %{creator: creator})
      {:ok, subscription} = Schedule.create_subscription(user.id, creator.id, medium.id)
      medium = get_media_subscription_count(medium)
      conn = build_conn()
      %{query: query, creator: creator, conn: conn, medium: medium, subscription: subscription}
    end

    test "returns a list of media", %{query: query, conn: conn, medium: medium} do
      actual_medium =
        conn
        |> post("/api", %{query: query})
        |> json_response(200)
        |> get_in(["data", "media"])
        |> List.first()

      assert actual_medium["id"] == to_string(medium.id)
      assert actual_medium["creatorId"] == to_string(medium.creator_id)
      assert actual_medium["name"] == medium.name
      assert actual_medium["type"] == to_string(medium.type)
      assert actual_medium["subscriptionCount"] == medium.subscription_count
    end
  end

  defp get_media_subscription_count(medium) do
    query = from(medium in Content.Media, as: :medium, where: medium.id == ^medium.id)

    query
    |> Content.Media.load_subscription_count()
    |> Repo.one()
  end
end
