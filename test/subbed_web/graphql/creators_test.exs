defmodule SubbedWeb.Graphql.CreatorsTest do
  use SubbedWeb.ConnCase
  alias Subbed.{Content, Schedule, Repo}
  import Ecto.Query
  import Plug.Test

  setup do
    creator = insert(:creator)
    user = insert(:user)
    {:ok, _} = Schedule.create_subscription(user.id, creator.id)
    {:ok, _maintainer} = Content.add_maintainer(creator.id, user.id)
    creator = get_creator_subscription_count(creator)

    conn =
      build_conn()
      |> init_test_session(%{user_id: user.id})

    on_exit(fn ->
      # Delete any images that exist in test/support/images
      dir = "test/support/images"

      dir
      |> File.ls!()
      |> Enum.filter(fn s -> not String.starts_with?(s, ".") end)
      |> Enum.map(fn path -> Path.expand(path, "test/support/images") end)
      |> Enum.each(&File.rm_rf!/1)
    end)

    %{creator: creator, conn: conn}
  end

  describe "list_creators" do
    setup do
      query = """
      query {
        creators {
          id
          name
          timezone
          subscriptionCount
        }
      }
      """

      %{query: query}
    end

    test "returns a list of creators", %{query: query, creator: creator, conn: conn} do
      response =
        conn
        |> post("/api", %{query: query})
        |> json_response(200)

      %{"creators" => [actual_creator]} = response["data"]
      assert_creator(actual_creator, creator)
    end

    test "returns a list of creators that a user maintains", %{creator: creator, conn: conn} do
      query = """
      query {
        creators(myCreatorsFilter: MAINTAINER) {
          id
          name
          timezone
          subscriptionCount
        }
      }
      """

      response =
        conn
        |> post("/api", %{query: query})
        |> json_response(200)

      %{"creators" => [actual_creator]} = response["data"]
      assert_creator(actual_creator, creator)
    end

    test "returns a list of creators that a user is subscribed to", %{
      creator: creator,
      conn: conn
    } do
      query = """
      query {
        creators(myCreatorsFilter: SUBSCRIBED_TO) {
          id
          name
          timezone
          subscriptionCount
        }
      }
      """

      response =
        conn
        |> post("/api", %{query: query})
        |> json_response(200)

      %{"creators" => [actual_creator]} = response["data"]
      assert_creator(actual_creator, creator)
    end
  end

  describe "get_creator" do
    setup do
      query = """
      query creator($id: ID, $name: String) {
        creator(id: $id, name: $name) {
          id
          name
          timezone
          subscriptionCount
        }
      }
      """

      %{query: query}
    end

    test "returns a creator by id", %{query: query, creator: creator, conn: conn} do
      variables = %{"id" => to_string(creator.id)}

      response =
        conn
        |> post("/api", %{query: query, variables: variables})
        |> json_response(200)

      %{"creator" => actual_creator} = response["data"]
      assert_creator(actual_creator, creator)
    end

    test "returns a creator by name", %{query: query, creator: creator, conn: conn} do
      variables = %{"name" => creator.name}

      response =
        conn
        |> post("/api", %{query: query, variables: variables})
        |> json_response(200)

      %{"creator" => actual_creator} = response["data"]
      assert_creator(actual_creator, creator)
    end
  end

  describe "mutation addCreatorAvatar" do
    setup do
      mutation = """
      mutation addCreatorAvatar($creatorId: ID!) {
        addCreatorAvatar(creatorId: $creatorId, avatar: "avatar_file") {
          avatar
          avatarThumbnail
        }
      }
      """

      image_file = "luc.jpg"

      upload = %Plug.Upload{
        content_type: "image/jpg",
        filename: "luc.jpg",
        path: Path.expand(image_file, "test/support")
      }

      non_maintainer_conn = build_conn()

      %{
        mutation: mutation,
        upload: upload,
        image_file: image_file,
        non_maintainer_conn: non_maintainer_conn
      }
    end

    test "allows you to upload an avatar for a creator", context do
      %{mutation: mutation, conn: conn, creator: creator, upload: upload} = context

      variables = %{"creatorId" => creator.id}

      results =
        conn
        |> post("/api", %{query: mutation, variables: variables, avatar_file: upload})
        |> json_response(200)
        |> get_in(["data", "addCreatorAvatar"])

      original_img_path =
        "http://localhost:4000/test/support/images/creator_avatars/#{creator.name}/original.jpg"

      thumbnail_img_path =
        "http://localhost:4000/test/support/images/creator_avatars/#{creator.name}/thumbnail.jpg"

      assert results["avatar"] == original_img_path
      assert results["avatarThumbnail"] == thumbnail_img_path
    end

    test "returns an unauthorized error if the user is not a creator", context do
      %{mutation: mutation, non_maintainer_conn: conn, creator: creator, upload: upload} = context

      variables = %{"creatorId" => creator.id}

      results =
        conn
        |> post("/api", %{query: mutation, variables: variables, avatar_file: upload})
        |> json_response(200)

      error = %{
        "locations" => [%{"column" => 0, "line" => 2}],
        "message" => "unauthorized",
        "path" => ["addCreatorAvatar"]
      }

      assert results["errors"] == [error]
      refute results["data"]["addCreatorAvatar"]
    end
  end

  @spec assert_creator(map(), Content.Creator.t()) :: nil
  defp assert_creator(actual_creator, creator) do
    assert actual_creator["id"] == to_string(creator.id)
    assert actual_creator["name"] == creator.name
    assert actual_creator["timezone"] == creator.timezone
    assert actual_creator["subscriptionCount"] == creator.subscription_count
  end

  defp get_creator_subscription_count(creator) do
    query = from(creator in Content.Creator, as: :creator, where: creator.id == ^creator.id)

    query
    |> Content.Creator.load_subscription_count()
    |> Repo.one()
  end
end
