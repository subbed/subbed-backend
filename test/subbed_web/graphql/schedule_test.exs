defmodule SubbedWeb.Graphql.ScheduleTest do
  use SubbedWeb.ConnCase
  alias Subbed.{Constants, Content, Repo, Schedule}
  import Plug.Test

  describe "mutation createSubscription" do
    setup do
      creator = insert(:creator)
      user = insert(:user)
      medium = insert(:medium, %{creator: creator})

      conn = init_test_session(build_conn(), %{user_id: user.id})

      mutation = """
      mutation createSubscription(
        $creatorId: Int!,
        $mediumId: Int,
        $subscriptionType: SubscriptionType
      ) {
        createSubscription(
          creatorId: $creatorId,
          mediumId: $mediumId,
          subscriptionType: $subscriptionType
        ) {
          id
        }
      }
      """

      %{creator: creator, user: user, conn: conn, mutation: mutation, medium: medium}
    end

    test "allows a logged in user to subscribe to a creator", context do
      %{creator: creator, user: user, conn: conn, mutation: mutation} = context
      # Assert the user isn't subscribed
      subscribed_creators = Content.list_creators(:subscribed_to, user)
      assert Enum.empty?(subscribed_creators)

      variables = %{"creatorId" => creator.id}

      subscription_id =
        conn
        |> post("/api", %{query: mutation, variables: variables})
        |> json_response(200)
        |> get_in(["data", "createSubscription", "id"])

      subscription = Schedule.get_subscription!(subscription_id)
      assert subscription.user_id == user.id
      assert subscription.creator_id == creator.id
      refute subscription.type
      refute subscription.medium_id

      [subscribed_creator] = Content.list_creators(:subscribed_to, user)
      assert subscribed_creator.id == creator.id
    end

    test "allows a logged in user to subscribe to a creator's medium", context do
      %{creator: creator, user: user, conn: conn, mutation: mutation, medium: medium} = context

      variables = %{"creatorId" => creator.id, "mediumId" => medium.id}

      subscription_id =
        conn
        |> post("/api", %{query: mutation, variables: variables})
        |> json_response(200)
        |> get_in(["data", "createSubscription", "id"])

      subscription = Schedule.get_subscription!(subscription_id)
      assert subscription.user_id == user.id
      assert subscription.creator_id == creator.id
      assert subscription.medium_id == medium.id
      refute subscription.type
    end

    test "allows a logged in user to subscribe to only a creator's new content", context do
      %{creator: creator, user: user, conn: conn, mutation: mutation} = context

      variables = %{
        "creatorId" => creator.id,
        "subscriptionType" => Constants.subscription_types().new_content
      }

      subscription_id =
        conn
        |> post("/api", %{query: mutation, variables: variables})
        |> json_response(200)
        |> get_in(["data", "createSubscription", "id"])

      subscription = Schedule.get_subscription!(subscription_id)
      assert subscription.user_id == user.id
      assert subscription.creator_id == creator.id
      assert subscription.type == :NEW_CONTENT
    end

    test "returns an error when a user isn't logged in", context do
      %{creator: creator, mutation: mutation} = context
      conn = build_conn()

      variables = %{"creatorId" => creator.id}

      response =
        conn
        |> post("/api", %{query: mutation, variables: variables})
        |> json_response(200)

      expected_error = %{
        "locations" => [%{"column" => 0, "line" => 6}],
        "message" => "Must be logged in to subscribe to creators",
        "path" => ["createSubscription"]
      }

      refute response["data"]["createSubscription"]
      assert response["errors"] == [expected_error]
    end

    test "returns an error for an invalid creator", context do
      %{conn: conn, mutation: mutation} = context

      variables = %{"creatorId" => 0}

      response =
        conn
        |> post("/api", %{query: mutation, variables: variables})
        |> json_response(200)

      expected_error = %{
        "locations" => [%{"column" => 0, "line" => 6}],
        "message" => "creator_id does not exist",
        "path" => ["createSubscription"]
      }

      refute response["data"]["createSubscription"]
      assert response["errors"] == [expected_error]
    end
  end

  describe "mutation deleteSubscription" do
    setup do
      creator = insert(:creator)
      user = insert(:user)
      medium = insert(:medium, %{creator: creator})
      {:ok, subscription} = Schedule.create_subscription(user.id, creator.id, medium.id)

      conn = init_test_session(build_conn(), %{user_id: user.id})

      mutation = """
      mutation deleteSubscription($creatorId: Int, $mediumId: Int) {
        deleteSubscription(creatorId: $creatorId, mediumId: $mediumId) {
          id
        }
      }
      """

      %{
        creator: creator,
        user: user,
        conn: conn,
        mutation: mutation,
        medium: medium,
        subscription: subscription
      }
    end

    test "allows a logged in user to unsubscribe to a creator", context do
      %{creator: creator, conn: conn, mutation: mutation, subscription: subscription} = context
      variables = %{"creatorId" => creator.id}

      [deleted_subscription] =
        conn
        |> post("/api", %{query: mutation, variables: variables})
        |> json_response(200)
        |> get_in(["data", "deleteSubscription"])

      assert to_string(subscription.id) == deleted_subscription["id"]
      refute Repo.get(Schedule.Subscription, subscription.id)
    end

    test "allows a logged in user to unsubscribe from a creator's media", context do
      %{medium: medium, conn: conn, mutation: mutation, subscription: subscription} = context

      variables = %{"mediumId" => medium.id}

      [deleted_subscription] =
        conn
        |> post("/api", %{query: mutation, variables: variables})
        |> json_response(200)
        |> get_in(["data", "deleteSubscription"])

      assert to_string(subscription.id) == deleted_subscription["id"]
      refute Repo.get(Schedule.Subscription, subscription.id)
    end

    test "returns an error when a user isn't logged in", context do
      %{medium: medium, mutation: mutation, subscription: subscription} = context

      conn = build_conn()
      variables = %{"mediumId" => medium.id}

      response =
        conn
        |> post("/api", %{query: mutation, variables: variables})
        |> json_response(200)

      expected_error = %{
        "locations" => [%{"column" => 0, "line" => 2}],
        "message" => "Must be logged in to unsubscribe from a creator",
        "path" => ["deleteSubscription"]
      }

      refute response["data"]["createSubscription"]
      assert response["errors"] == [expected_error]
      assert Repo.get(Schedule.Subscription, subscription.id)
    end

    test "returns an error for an invalid medium id", context do
      %{conn: conn, mutation: mutation, subscription: subscription} = context

      variables = %{"mediumId" => 0}

      response =
        conn
        |> post("/api", %{query: mutation, variables: variables})
        |> json_response(200)

      expected_error = %{
        "locations" => [%{"column" => 0, "line" => 2}],
        "message" => "Cannot find something to unsubscribe from",
        "path" => ["deleteSubscription"]
      }

      assert response["errors"] == [expected_error]
      assert Repo.get(Schedule.Subscription, subscription.id)
    end
  end
end
