defmodule SubbedWeb.Graphql.EventsTest do
  use SubbedWeb.ConnCase
  alias Subbed.{Repo, Schedule, Test.Support}
  import Ecto.Query
  import Plug.Test

  setup do
    user = insert(:user)
    medium_1 = insert(:medium)
    medium_2 = insert(:medium, %{type: "video"})
    creator_1 = insert(:creator)
    creator_2 = insert(:creator)
    platform_1 = insert(:platform)
    platform_2 = insert(:platform)

    conn = init_test_session(build_conn(), %{user_id: user.id})

    %{
      conn: conn,
      user: user,
      medium_1: medium_1,
      medium_2: medium_2,
      creator_1: creator_1,
      creator_2: creator_2,
      platform_1: platform_1,
      platform_2: platform_2
    }
  end

  describe "query events" do
    setup context do
      query = """
      query events(
        $startDate: Date,
        $endDate: Date,
        $startTime: Time,
        $endTime: Time,
        $mediaType: MediaType,
        $sortBy: EventSortField,
        $withSubscription: Boolean
      ) {
        events(
          startDate: $startDate,
          endDate: $endDate,
          startTime: $startTime,
          endTime: $endTime,
          mediaType: $mediaType,
          sortBy: $sortBy,
          withSubscription: $withSubscription
        ) {
          medium {
            id
            name
            type
          }
          creator {
            id
          }
          description
          startDate
          endDate
          startTime
          endTime
          insertedAt
          id
          name
        }
      }
      """

      %{
        medium_1: medium_1,
        medium_2: medium_2,
        creator_1: creator_1,
        creator_2: creator_2,
        platform_1: platform_1,
        platform_2: platform_2
      } = context

      owner = insert(:user)

      now = Timex.now()
      {:ok, creator_1_start_time} = Time.new(10, 0, 0)
      {:ok, creator_1_end_time} = Time.new(11, 0, 0)
      {:ok, creator_2_start_time} = Time.new(11, 0, 0)
      {:ok, creator_2_end_time} = Time.new(12, 0, 0)
      start_date = Timex.to_date(now)
      end_date = now |> Timex.shift(weeks: 1) |> Timex.to_date()

      creator_platforms = [
        {creator_1, platform_1, creator_1_start_time, creator_1_end_time, medium_1},
        {creator_2, platform_2, creator_2_start_time, creator_2_end_time, medium_2}
      ]

      # Create 1 hour daily events
      [creator_1_recurring_event, creator_2_recurring_event] =
        Enum.map(creator_platforms, fn {creator, platform, start_time, end_time, medium} ->
          Support.Schedule.create_recurring_event(%{
            owner: owner,
            creator: creator,
            medium: medium,
            platform: platform,
            start_time: start_time,
            end_time: end_time,
            start_date: start_date,
            end_date: end_date
          })
        end)

      %{
        query: query,
        creator_1: creator_1,
        creator_1_recurring_event: creator_1_recurring_event,
        creator_2: creator_2,
        creator_2_recurring_event: creator_2_recurring_event
      }
    end

    test "returns a list of events for the next 7 days when no arguments are provided", context do
      %{
        medium_1: medium_1,
        medium_2: medium_2,
        conn: conn,
        query: query,
        creator_1: creator_1,
        creator_2: creator_2
      } = context

      response =
        conn
        |> post("/api", %{query: query})
        |> json_response(200)

      %{"events" => events} = response["data"]
      assert Enum.count(events) == 16

      start_time = Timex.now()
      start_date = Timex.to_date(start_time)

      end_time =
        start_time
        |> Timex.end_of_day()
        |> Schedule.Event.to_time!()

      end_date =
        start_time
        |> Timex.shift(weeks: 1)
        |> Timex.to_date()

      expected_events = Schedule.list_events(start_date, end_date, start_time, end_time)

      {creator_1_expected_events, creator_2_expected_events} =
        Enum.split_with(expected_events, fn event ->
          event.creator_id == creator_1.id
        end)

      {creator_1_actual_events, creator_2_actual_events} =
        Enum.split_with(events, fn event ->
          event["creator"]["id"] == to_string(creator_1.id)
        end)

      creator_1_matched_events = Enum.zip(creator_1_actual_events, creator_1_expected_events)
      creator_2_matched_events = Enum.zip(creator_2_actual_events, creator_2_expected_events)

      Enum.each(creator_1_matched_events, fn {actual_event, expected_event} ->
        assert_events_match(actual_event, expected_event, medium_1, creator_1)
      end)

      Enum.each(creator_2_matched_events, fn {actual_event, expected_event} ->
        assert_events_match(actual_event, expected_event, medium_2, creator_2)
      end)
    end

    test "return events that only fall within the specified timeframe", context do
      %{
        conn: conn,
        creator_1: creator_1,
        medium_1: medium_1,
        query: query
      } = context

      {:ok, start_time} = Time.new(10, 0, 0)
      {:ok, end_time} = Time.new(11, 0, 0)
      start_date = Timex.to_date(Timex.now())

      variables = %{
        "startTime" => Time.to_string(start_time),
        "startDate" => Date.to_string(start_date),
        "endTime" => Time.to_string(end_time),
        "endDate" => Date.to_string(start_date)
      }

      [creator_1_actual_event] =
        conn
        |> post("/api", %{query: query, variables: variables})
        |> json_response(200)
        |> get_in(["data", "events"])

      [creator_1_expected_event] =
        Schedule.list_events(start_date, start_date, start_time, end_time)

      assert_events_match(
        creator_1_actual_event,
        creator_1_expected_event,
        medium_1,
        creator_1
      )
    end

    test "allows you to filter by mediaType", context do
      %{conn: conn, query: query} = context

      events =
        conn
        |> post("/api", %{query: query, variables: %{"mediaType" => "PODCAST"}})
        |> json_response(200)
        |> get_in(["data", "events"])

      Enum.each(events, fn event ->
        actual_media_type = get_in(event, ["medium", "type"])
        assert actual_media_type == "podcast"
      end)
    end

    test "allows you to order events by insertion date", context do
      %{conn: conn, query: query, creator_2_recurring_event: recurring_event} = context
      new_inserted_at = Timex.shift(recurring_event.inserted_at, hours: 1)

      update_inserted_at =
        from(event in Schedule.Event,
          where: event.id == ^recurring_event.id,
          update: [set: [inserted_at: ^new_inserted_at]]
        )

      {1, _} = Repo.update_all(update_inserted_at, [])

      recurring_event = Repo.get(Schedule.Event, recurring_event.id)
      assert Timex.equal?(new_inserted_at, recurring_event.inserted_at)

      events =
        conn
        |> post("/api", %{query: query, variables: %{"sortBy" => "INSERTED_AT"}})
        |> json_response(200)
        |> get_in(["data", "events"])

      assert events_preceed_one_another?(events)
    end

    test "returns events that for content that a user is subscribed to", context do
      %{conn: conn, creator_1: creator, query: query, user: user} = context
      {:ok, _} = Schedule.create_subscription(user.id, creator.id)

      events =
        conn
        |> post("/api", %{query: query, variables: %{"withSubscription" => true}})
        |> json_response(200)
        |> get_in(["data", "events"])

      Enum.each(events, fn event ->
        creator_id = get_in(event, ["creator", "id"])
        assert creator_id == to_string(creator.id)
      end)
    end

    defp events_preceed_one_another?(events) do
      result =
        events
        |> Enum.map(fn event -> NaiveDateTime.from_iso8601!(event["insertedAt"]) end)
        |> Enum.reduce_while({nil, true}, fn
          event, {nil, _} ->
            {:cont, {event, true}}

          event, {prev_event, _} ->
            cond do
              Timex.equal?(prev_event, event) ->
                {:cont, {event, true}}

              Timex.before?(prev_event, event) ->
                {:cont, {event, true}}

              true ->
                {:halt, false}
            end
        end)

      case result do
        false -> false
        {_, true} -> true
      end
    end

    defp assert_events_match(actual_event, event, medium, creator) do
      actual_event_medium = actual_event["medium"]
      actual_event_creator = actual_event["creator"]

      assert actual_event["description"] == event.description
      assert actual_event["startDate"] == Date.to_string(event.start_date)
      assert actual_event["endDate"] == Date.to_string(event.end_date)
      assert actual_event["startTime"] == Time.to_string(event.start_time)
      assert actual_event["endTime"] == Time.to_string(event.end_time)
      assert actual_event_medium["id"] == to_string(medium.id)
      assert actual_event_medium["name"] == medium.name
      assert actual_event_medium["type"] == to_string(medium.type)
      assert actual_event_creator["id"] == to_string(creator.id)
    end
  end

  describe "mutation createEvent" do
    setup context do
      %{
        creator_1: creator,
        medium_1: medium
      } = context

      mutation = """
        mutation createEvent($event: EventInput!) {
          createEvent(event: $event) {
            id
          }
        }
      """

      now = Timex.now()

      today = Timex.to_date(now)
      today_str = Date.to_string(today)

      start_datetime =
        Timex.to_datetime({{today.year, today.month, today.day}, {17, 0, 0}}, "Etc/UTC")

      {:ok, start_time} = Schedule.Event.to_time(start_datetime)

      {:ok, end_time} = start_datetime |> Timex.shift(hours: 1) |> Schedule.Event.to_time()

      event_params = %{
        "creatorId" => creator.id,
        "mediumId" => medium.id,
        "startTime" => Time.to_string(start_time),
        "endTime" => Time.to_string(end_time),
        "startDate" => today_str,
        "endDate" => today_str,
        "platform" => "TWITCH"
      }

      %{
        end_time: end_time,
        event_params: event_params,
        mutation: mutation,
        start_datetime: start_datetime,
        start_time: start_time
      }
    end

    test "allows you to create a single non-recurring event", context do
      %{
        conn: conn,
        end_time: end_time,
        event_params: event_params,
        mutation: mutation,
        start_time: start_time,
        creator_1: creator
      } = context

      variables = %{
        "event" => event_params
      }

      response =
        conn
        |> post("/api", %{query: mutation, variables: variables})
        |> json_response(200)

      refute response["errors"]
      %{"createEvent" => created_event} = response["data"]
      event = Repo.get!(Schedule.Event, created_event["id"])

      now = Timex.now()
      today = Timex.to_date(now)

      assert event.creator_id == creator.id
      assert event.creator_id == creator.id
      assert event.start_date == today
      assert event.end_date == today
      assert event.start_time == start_time
      assert event.end_time == end_time
      refute event.is_recurring
    end

    test "allows you to create a recurring event that occurs every 2 days", context do
      %{
        conn: conn,
        event_params: event_params,
        mutation: mutation
      } = context

      start_date = Timex.to_date(Timex.now())
      end_date = Timex.now() |> Timex.shift(days: 10) |> Timex.to_date()

      recurring_params = %{
        "isRecurring" => true,
        "recurringType" => "DAILY",
        "separationCount" => 2
      }

      event_params = Map.put(event_params, "endDate", Date.to_string(end_date))
      variables = %{"event" => Map.merge(event_params, recurring_params)}

      response =
        conn
        |> post("/api", %{query: mutation, variables: variables})
        |> json_response(200)

      # We don't actually need this ID since it doesn't tell us anything
      event_id =
        response
        |> Map.fetch!("data")
        |> Map.fetch!("createEvent")
        |> Map.fetch!("id")

      # Assert the event and pattern were created
      recurring_event = Schedule.Event |> Repo.get(event_id) |> Repo.preload(:recurring_pattern)
      pattern = recurring_event.recurring_pattern

      assert recurring_event.is_recurring
      assert pattern.id > 0
      assert pattern.separation_count == 2
      assert pattern.recurring_type == :daily
      assert is_nil(pattern.day_of_month)
      assert is_nil(pattern.day_of_week)
      assert is_nil(pattern.month_of_year)
      assert is_nil(pattern.week_of_month)

      events = Schedule.list_events(start_date, end_date)
      assert Enum.count(events) == 6
    end
  end
end
