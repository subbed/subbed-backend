defmodule SubbedWeb.Graphql.PlatformsTest do
  use SubbedWeb.ConnCase

  describe "list_platforms" do
    setup do
      query = """
      query {
        platforms {
          id
          name
          url
        }
      }
      """

      platform = insert(:platform)
      conn = build_conn()
      %{query: query, platform: platform, conn: conn}
    end

    test "returns a list of platforms", %{query: query, platform: platform, conn: conn} do
      response =
        conn
        |> post("/api", %{query: query})
        |> json_response(200)

      %{"platforms" => actual_platforms} = response["data"]
      actual_platform = List.last(actual_platforms)
      assert actual_platform["id"] == to_string(platform.id)
      assert actual_platform["url"] == platform.url
      assert actual_platform["name"] == platform.name
    end
  end
end
