defmodule Subbed.Factory do
  use ExMachina.Ecto, repo: Subbed.Repo
  alias Subbed.{Account, Content, Schedule}

  def platform_factory do
    platform_namer = fn id -> "platform-#{id}" end

    %Schedule.Platform{
      name: sequence(:platform, platform_namer),
      url: "https://subbed.io"
    }
  end

  def user_factory do
    user_id_namer = fn id -> "user_id-#{id}" end
    %Account.User{user_id: sequence(:user_id, user_id_namer)}
  end

  def creator_factory do
    creator_namer = fn id -> "creator-#{id}" end

    %Content.Creator{
      name: sequence(:creator, creator_namer),
      timezone: "America/Los_Angeles"
    }
  end

  def event_factory do
    creator = insert(:creator)
    owner = insert(:user)
    medium = insert(:medium)
    event_namer = fn id -> "event-#{id}" end
    description_namer = fn id -> "event_description-#{id}" end
    now = Timex.now()
    {:ok, start_time} = Schedule.Event.to_time(now)
    start_date = Timex.to_date(now)
    end_time = Timex.shift(now, hours: 1)
    end_date = Timex.to_date(end_time)
    platform = insert(:platform)

    %Schedule.Event{
      name: sequence(:event_name, event_namer),
      description: sequence(:event_description, description_namer),
      owner: owner,
      creator: creator,
      medium: medium,
      platform: platform.name,
      start_time: start_time,
      start_date: start_date,
      end_time: end_time,
      end_date: end_date
    }
  end

  @type medium_factory_args :: %{creator: Content.Creator.t()}
  @spec medium_factory(medium_factory_args()) :: Content.Media.t()
  def medium_factory(attrs \\ %{}) do
    medium_namer = fn id -> "medium-#{id}" end
    creator = Map.get(attrs, :creator, insert(:creator))

    %Content.Media{
      creator: creator,
      name: sequence(:medium, medium_namer),
      type: "podcast"
    }
  end
end
