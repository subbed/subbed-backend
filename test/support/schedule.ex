defmodule Subbed.Test.Support.Schedule do
  alias Subbed.{Account, Content, Schedule, Repo}
  import Subbed.Factory

  @doc """
  Helper function for creating a recurring event.
  You can pass absolutely nothing to this function and it'll create a 1 hour
  recurring event that starts at 10AM UTC and ends at 11AM UTC. The event starts
  on Feb 5th and ends on Feb 12th.

  Options:
  Please see `create_recurring_event_options()` for the types that this function
  accepts.

  These are the options this function accepts: 
  * :owner - Set the owner of the event
  * :creator - Set the creator of the event
  * :medium - Set the medium of the event
  * :platform - Set the platform of the event
  * :start_date - The date which the event should start on. Defaults to 2/5/20.
  * :start_time - The time which the event should start on. Defaults to 10AM UTC
  * :end_date - The date which the event should start on. Defaults to 2/12/20.
  * :end_time - The time which the event should start on. Defaults to 11AM UTC
  """
  @type create_recurring_event_options() :: %{
          optional(:owner) => Account.User.t(),
          optional(:creator) => Content.Creator.t(),
          optional(:medium) => Content.Medium.t(),
          optional(:platform) => Schedule.Platform.t(),
          optional(:start_date) => Date.t(),
          optional(:start_time) => Time.t(),
          optional(:end_date) => Date.t(),
          optional(:end_time) => Time.t()
        }
  @spec create_recurring_event(create_recurring_event_options()) :: Schedule.Event.t()
  def create_recurring_event(attrs \\ %{}) do
    # Associations
    owner = Map.get(attrs, :owner, insert(:user))
    creator = Map.get(attrs, :creator, insert(:creator))
    medium = Map.get(attrs, :medium, insert(:medium, %{creator: creator}))
    platform = Map.get(attrs, :platform, insert(:platform))

    # Event Attributes
    start_date = Map.get(attrs, :start_date, ~D[2020-02-05])
    end_date = Map.get(attrs, :end_date, ~D[2020-02-12])

    {:ok, default_start_time} = Time.new(10, 0, 0, 0)
    {:ok, default_end_time} = Time.new(11, 0, 0, 0)
    start_time = Map.get(attrs, :start_time, default_start_time)
    end_time = Map.get(attrs, :end_time, default_end_time)

    params = %Schedule.Event.Params{
      owner_id: owner.id,
      medium_id: medium.id,
      creator_id: creator.id,
      platform: platform.name,
      end_time: end_time,
      end_date: end_date,
      start_time: start_time,
      start_date: start_date
    }

    {:ok, recurring_event} = Schedule.create_recurring_event(params, 1, :daily)
    Repo.preload(recurring_event, [:owner, :creator, :medium])
  end
end
