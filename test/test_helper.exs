# Seed Platforms
Subbed.Seeders.Platforms.seed()

ExUnit.start()
Ecto.Adapters.SQL.Sandbox.mode(Subbed.Repo, :manual)

{:ok, _} = Application.ensure_all_started(:ex_machina)
