defmodule Subbed.Test.Schedule.EventsTest do
  use Subbed.DataCase
  require Integer

  alias Subbed.{Schedule, Test.Support}

  describe "recurring events" do
    setup do
      owner = insert(:user)
      creator = insert(:creator)
      platform = insert(:platform)
      medium = insert(:medium)

      {:ok, start_time} = Time.new(10, 0, 0)
      {:ok, end_time} = Time.new(11, 0, 0)

      start_date = Timex.to_date(Timex.now())
      end_date = Timex.to_date(Timex.now())

      params = %Schedule.Event.Params{
        owner_id: owner.id,
        medium_id: medium.id,
        creator_id: creator.id,
        platform: platform.name,
        end_time: end_time,
        end_date: end_date,
        start_time: start_time,
        start_date: start_date
      }

      %{platform: platform, owner: owner, creator: creator, params: params}
    end

    test "create_event/1 lets you create a recurring event that occurs every 2 days", context do
      %{params: params} = context

      # Daily Recurrence
      # Occurs every 2 days
      {:ok, recurring_event} = Schedule.create_recurring_event(params, 2, :daily)
      pattern = recurring_event.recurring_pattern

      assert recurring_event.is_recurring
      assert pattern.id > 0
      assert pattern.separation_count == 2
      assert pattern.recurring_type == :daily
      assert is_nil(pattern.day_of_month)
      assert is_nil(pattern.day_of_week)
      assert is_nil(pattern.month_of_year)
      assert is_nil(pattern.week_of_month)
    end

    # Weekly Recurrence
    test "create_event/1 lets you create a recurring event that occurs weekly every tuesday",
         context do
      %{params: params} = context

      # Start date must be a tuesday
      start_date = ~D[2020-02-18]
      end_date = ~D[2020-03-18]

      params = Map.merge(params, %{start_date: start_date, end_date: end_date})

      # Occurs weekly every tue
      {:ok, recurring_event} =
        Schedule.create_recurring_event(params, 1, :weekly,
          day_of_week: Schedule.day_of_week().tuesday
        )

      pattern = recurring_event.recurring_pattern

      assert pattern.id > 0
      assert pattern.separation_count == 1
      assert pattern.recurring_type == :weekly
      assert pattern.day_of_week == Schedule.day_of_week().tuesday
      assert is_nil(pattern.day_of_month)
      assert is_nil(pattern.month_of_year)
      assert is_nil(pattern.week_of_month)

      # We should now list all the events and ensure they occur weekly on tuesday
      events = Schedule.list_events(start_date, end_date)
      actual_dates = Enum.map(events, fn event -> event.start_date end)

      expected_dates = [
        ~D[2020-02-18],
        ~D[2020-02-25],
        ~D[2020-03-03],
        ~D[2020-03-10],
        ~D[2020-03-17]
      ]

      assert actual_dates == expected_dates
    end

    # Monthly Recurrence
    test "create_event/1 lets you create a recurring event that occurs on the 11th of every month",
         context do
      %{params: params} = context

      start_date = ~D[2020-02-11]
      end_date = ~D[2020-06-11]
      params = Map.merge(params, %{start_date: start_date, end_date: end_date})

      {:ok, recurring_event} =
        Schedule.create_recurring_event(params, 1, :monthly, day_of_month: 11)

      pattern = recurring_event.recurring_pattern

      assert pattern.id > 0
      assert pattern.separation_count == 1
      assert pattern.recurring_type == :monthly
      assert is_nil(pattern.day_of_week)
      assert pattern.day_of_month == 11
      assert is_nil(pattern.month_of_year)
      assert is_nil(pattern.week_of_month)

      events = Schedule.list_events(start_date, end_date)
      actual_dates = Enum.map(events, fn event -> event.start_date end)

      expected_dates = [
        ~D[2020-02-11],
        ~D[2020-03-11],
        ~D[2020-04-11],
        ~D[2020-05-11],
        ~D[2020-06-11]
      ]

      assert actual_dates == expected_dates
    end

    test "create_event/1 lets you create a recurring event that " <>
           "occurs on the fourth Thursday of every month",
         context do
      %{params: params} = context

      start_date = ~D[2020-02-27]
      end_date = ~D[2020-06-25]
      params = Map.merge(params, %{start_date: start_date, end_date: end_date})

      {:ok, recurring_event} =
        Schedule.create_recurring_event(params, 1, :monthly,
          week_of_month: 4,
          day_of_week: Schedule.day_of_week().thursday
        )

      pattern = recurring_event.recurring_pattern

      assert pattern.id > 0
      assert pattern.separation_count == 1
      assert pattern.recurring_type == :monthly
      assert pattern.week_of_month == 4
      assert pattern.day_of_week == Schedule.day_of_week().thursday
      assert is_nil(pattern.day_of_month)
      assert is_nil(pattern.month_of_year)

      events = Schedule.list_events(start_date, end_date)
      actual_dates = Enum.map(events, fn event -> event.start_date end)

      expected_dates = [
        ~D[2020-02-27],
        ~D[2020-03-26],
        ~D[2020-04-23],
        ~D[2020-05-28],
        ~D[2020-06-25]
      ]

      assert actual_dates == expected_dates
    end

    # Yearly Recurrence
    test "create_event/1 lets you create a yearly recurring event like xmas (12/25) ",
         context do
      %{params: params} = context

      params = Map.merge(params, %{start_date: ~D[2020-12-25], end_date: ~D[2025-12-25]})

      {:ok, recurring_event} =
        Schedule.create_recurring_event(params, 1, :yearly,
          day_of_month: 25,
          month_of_year: Schedule.month_of_year().december
        )

      pattern = recurring_event.recurring_pattern

      assert pattern.id > 0
      assert pattern.separation_count == 1
      assert pattern.recurring_type == :yearly
      assert pattern.day_of_month == 25
      assert pattern.month_of_year == Schedule.month_of_year().december
      assert is_nil(pattern.week_of_month)
      assert is_nil(pattern.day_of_week)

      events = Schedule.list_events(~D[2020-01-01], ~D[2025-01-01])
      actual_events = Enum.map(events, fn event -> event.start_date end)

      expected_events = [
        ~D[2020-12-25],
        ~D[2021-12-25],
        ~D[2022-12-25],
        ~D[2023-12-25],
        ~D[2024-12-25]
      ]

      assert actual_events == expected_events
    end

    # # Thanksgiving
    test "create_event/1 lets you create a yearly recurring event that " <>
           "occurs on the Thursday of the 4th week of November",
         context do
      %{params: params} = context

      params = Map.merge(params, %{start_date: ~D[2020-11-26], end_date: ~D[2025-11-27]})

      {:ok, recurring_event} =
        Schedule.create_recurring_event(params, 1, :yearly,
          week_of_month: 4,
          day_of_week: Schedule.day_of_week().thursday,
          month_of_year: Schedule.month_of_year().november
        )

      pattern = recurring_event.recurring_pattern

      assert pattern.id > 0
      assert pattern.separation_count == 1
      assert pattern.recurring_type == :yearly
      assert pattern.week_of_month == 4
      assert pattern.day_of_week == Schedule.day_of_week().thursday
      assert pattern.month_of_year == Schedule.month_of_year().november
      assert is_nil(pattern.day_of_month)

      events = Schedule.list_events(~D[2020-11-26], ~D[2025-11-27])
      actual_events = Enum.map(events, fn event -> event.start_date end)

      expected_dates = [
        ~D[2020-11-26],
        ~D[2021-11-25],
        ~D[2022-11-24],
        ~D[2023-11-23],
        ~D[2024-11-28],
        ~D[2025-11-27]
      ]

      assert actual_events == expected_dates
    end

    test "returns an error for an invalid recurring daily event", context do
      %{params: params} = context
      {status, changeset} = Schedule.create_recurring_event(params, 2, :daily, week_of_month: 2)
      assert status == :error
      refute changeset.valid?

      Ecto.Changeset.traverse_errors(changeset, fn {msg, _opts} ->
        assert msg == "Invalid parameters provided for the recurring type."
      end)
    end

    test "returns an error for an invalid recurring weekly event", context do
      %{params: params} = context
      {status, changeset} = Schedule.create_recurring_event(params, 2, :weekly, week_of_month: 2)
      assert status == :error
      refute changeset.valid?

      Ecto.Changeset.traverse_errors(changeset, fn {msg, _opts} ->
        assert msg == "Invalid parameters provided for the recurring type."
      end)
    end

    test "returns an error for an invalid recurring monthly event", context do
      %{params: params} = context

      {status, changeset} =
        Schedule.create_recurring_event(params, 2, :weekly,
          day_of_month: 2,
          day_of_week: Schedule.day_of_week().tuesday
        )

      assert status == :error
      refute changeset.valid?

      Ecto.Changeset.traverse_errors(changeset, fn {msg, _opts} ->
        assert msg == "Invalid parameters provided for the recurring type."
      end)
    end

    test "creating an overnight event sets the end_date to the next date", context do
      %{params: params} = context
      start_date = ~D[2020-02-21]
      end_date = ~D[2020-02-23]

      params =
        Map.merge(params, %{
          start_time: ~T[18:30:00],
          end_time: ~T[03:30:00],
          start_date: start_date,
          end_date: end_date
        })

      {:ok, _recurring_event} = Schedule.create_recurring_event(params, 1, :daily)
      events = Schedule.list_events(start_date, end_date)

      actual_start_end_tuples =
        Enum.map(events, fn event ->
          {{event.start_date, event.start_time}, {event.end_date, event.end_time}}
        end)

      expected_start_end_tuples = [
        {
          {~D[2020-02-21], ~T[18:30:00]},
          {~D[2020-02-22], ~T[03:30:00]}
        },
        {
          {~D[2020-02-22], ~T[18:30:00]},
          {~D[2020-02-23], ~T[03:30:00]}
        },
        {
          {~D[2020-02-23], ~T[18:30:00]},
          {~D[2020-02-24], ~T[03:30:00]}
        }
      ]

      assert actual_start_end_tuples == expected_start_end_tuples
    end
  end

  describe "events" do
    alias Subbed.Schedule.Event

    @invalid_attrs %{description: nil, end_time: nil, start_time: nil}

    setup do
      event = insert(:event)
      insert(:event)
      %{event: event}
    end

    test "list_events/1 returns all events" do
      assert Schedule.list_events(%{}) == Repo.all(Event)
    end

    test "get_event!/1 returns the event with given id", %{event: event} do
      expected_event = Schedule.get_event!(event.id)
      assert expected_event.id == event.id
    end

    test "create_event/1 with valid data creates a event", %{event: event} do
      now = Timex.now()
      {:ok, start_time} = Schedule.Event.to_time(now)
      {:ok, end_time} = now |> Timex.shift(hours: 1) |> Schedule.Event.to_time()

      start_date = Timex.to_date(now)
      end_date = Timex.to_date(now)

      params = %{
        owner_id: event.owner_id,
        creator_id: event.creator_id,
        medium_id: event.medium_id,
        platform: event.platform,
        end_time: end_time,
        end_date: end_date,
        start_time: start_time,
        start_date: start_date
      }

      {:ok, %Event{} = new_event} = Schedule.create_event(params)

      {:ok, actual_start_time} = Schedule.Event.to_time(new_event.start_time)
      {:ok, actual_end_time} = Schedule.Event.to_time(new_event.end_time)
      assert actual_start_time == start_time
      assert actual_end_time == end_time
    end

    test "create_event/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Schedule.create_event(@invalid_attrs)
    end

    test "update_event/2 with valid data updates the event", %{event: event} do
      now = Timex.now()
      {:ok, start_time} = Schedule.Event.to_time(now)
      {:ok, end_time} = now |> Timex.shift(hours: 1) |> Schedule.Event.to_time()

      description = "foo"
      name = "bar"

      params = %{
        name: name,
        description: description,
        end_time: end_time,
        start_time: start_time
      }

      assert {:ok, %Event{} = event} = Schedule.update_event(event, params)
      assert event.description == description

      {:ok, actual_start_time} = Schedule.Event.to_time(event.start_time)
      {:ok, actual_end_time} = Schedule.Event.to_time(event.end_time)
      assert actual_start_time == start_time
      assert actual_end_time == end_time
    end

    test "update_event/2 with invalid data returns error changeset", %{event: event} do
      assert {:error, %Ecto.Changeset{}} = Schedule.update_event(event, @invalid_attrs)
    end

    test "delete_event/1 deletes the event", %{event: event} do
      assert {:ok, %Event{}} = Schedule.delete_event(event)
      assert_raise Ecto.NoResultsError, fn -> Schedule.get_event!(event.id) end
    end

    test "change_event/1 returns a event changeset", %{event: event} do
      assert %Ecto.Changeset{} = Schedule.change_event(event)
    end

    test "timestamps should be in utc timezone", %{event: event} do
      # Converting it to a UTC timezone should yield the same timestamp
      assert Timex.to_datetime(event.inserted_at, :utc) == event.inserted_at
    end
  end

  describe "Schedule.list_events/5" do
    setup do
      owner = insert(:user)
      medium = insert(:medium)
      platform_1 = insert(:platform)
      platform_2 = insert(:platform)

      creator_1_event = Support.Schedule.create_recurring_event()

      {:ok, creator_2_start_time} = Time.new(11, 0, 0, 0)
      {:ok, creator_2_end_time} = Time.new(12, 0, 0, 0)

      creator_2_event =
        Support.Schedule.create_recurring_event(%{
          start_time: creator_2_start_time,
          end_time: creator_2_end_time
        })

      %{
        medium: medium,
        owner: owner,
        platform_1: platform_1,
        platform_2: platform_2,
        creator_1: creator_1_event.creator,
        creator_1_end_time: creator_1_event.end_time,
        creator_1_event: creator_1_event,
        creator_1_start_time: creator_1_event.start_time,
        creator_2: creator_2_event.creator,
        creator_2_end_time: creator_2_end_time,
        creator_2_event: creator_2_event,
        creator_2_start_time: creator_2_start_time
      }
    end

    test "returns all the events for the given range", context do
      %{
        creator_1_event: creator_1_event,
        creator_2_event: creator_2_event,
        creator_1: creator_1,
        creator_2: creator_2
      } = context

      start_date = creator_1_event.start_date

      event_end_date =
        start_date
        |> Timex.shift(days: 4)
        |> Timex.to_date()

      creator_2_start_time = creator_2_event.start_time
      creator_2_end_time = creator_2_event.end_time

      platform_1 = creator_1_event.platform
      platform_2 = creator_2_event.platform

      events =
        Schedule.list_events(
          start_date,
          event_end_date,
          creator_1_event.start_time,
          creator_2_event.end_time
        )

      assert Enum.count(events) == 10
      creator_1_events = Enum.filter(events, fn event -> event.creator_id == creator_1.id end)
      creator_2_events = Enum.filter(events, fn event -> event.creator_id == creator_2.id end)
      assert Enum.count(creator_1_events) == 5
      assert Enum.count(creator_2_events) == 5

      # Because Creator 1 events occur an hour before Creator 2
      # They should be alternating in the list
      for n <- 0..9 do
        # Even should be creator 1
        # Odd should be creator 2
        event = Enum.at(events, n)
        creator_id = if Integer.is_even(n), do: creator_1.id, else: creator_2.id
        assert event.creator_id == creator_id
      end

      for day_shift <- 0..4 do
        creator_1_event = Enum.at(creator_1_events, day_shift)
        expected_start_date = Timex.shift(start_date, days: day_shift)

        creator_1_expected_values = %{
          expected_start_time: creator_1_event.start_time,
          expected_end_time: creator_1_event.end_time,
          expected_start_date: expected_start_date,
          expected_end_date: expected_start_date,
          expected_platform: platform_1,
          expected_creator_id: creator_1.id
        }

        assert_recurring_event(creator_1_event, creator_1_expected_values)

        creator_2_event = Enum.at(creator_2_events, day_shift)

        creator_2_expected_values = %{
          expected_start_time: creator_2_start_time,
          expected_end_time: creator_2_end_time,
          expected_start_date: expected_start_date,
          expected_end_date: expected_start_date,
          expected_platform: platform_2,
          expected_creator_id: creator_2.id
        }

        assert_recurring_event(creator_2_event, creator_2_expected_values)
      end
    end

    test "returns the correct number of event when the start and end date are the same",
         context do
      %{
        creator_1: creator_1,
        creator_1_end_time: creator_1_end_time,
        creator_1_start_time: creator_1_start_time,
        creator_1_event: creator_1_event,
        creator_2: creator_2,
        creator_2_end_time: creator_2_end_time,
        creator_2_event: creator_2_event,
        creator_2_start_time: creator_2_start_time
      } = context

      events =
        Schedule.list_events(
          creator_1_event.start_date,
          creator_1_event.start_date
        )

      # There should only be 2 events, 1 for each creator
      assert Enum.count(events) == 2
      [actual_creator_1_event, actual_creator_2_event] = events

      creator_1_expected_values = %{
        expected_start_time: creator_1_start_time,
        expected_end_time: creator_1_end_time,
        expected_start_date: creator_1_event.start_date,
        expected_end_date: creator_1_event.start_date,
        expected_platform: creator_1_event.platform,
        expected_creator_id: creator_1.id
      }

      assert_recurring_event(actual_creator_1_event, creator_1_expected_values)

      creator_2_expected_values = %{
        expected_start_time: creator_2_start_time,
        expected_end_time: creator_2_end_time,
        expected_start_date: creator_2_event.start_date,
        expected_end_date: creator_2_event.start_date,
        expected_platform: creator_2_event.platform,
        expected_creator_id: creator_2.id
      }

      assert_recurring_event(actual_creator_2_event, creator_2_expected_values)
    end

    test "returns all events for the given hour range", context do
      %{creator_1_event: creator_1_event} = context
      %{start_date: start_date, start_time: start_time, end_time: end_time} = creator_1_event

      events = Schedule.list_events(start_date, start_date, start_time, end_time)

      assert Enum.count(events) == 1
    end

    test "returns all events for the next day in the specified range", context do
      %{creator_1_event: creator_1_event} = context
      {:ok, start_time} = Time.new(0, 0, 0)
      {:ok, end_time} = Time.new(23, 59, 59)

      events =
        Schedule.list_events(
          creator_1_event.start_date,
          creator_1_event.start_date,
          start_time,
          end_time
        )

      assert Enum.count(events) == 2
    end

    test "returns all events for the next week in the specified range", context do
      %{creator_1_event: creator_1_event} = context
      {:ok, start_time} = Time.new(0, 0, 0)
      {:ok, end_time} = Time.new(23, 59, 59)
      start_date = Timex.shift(creator_1_event.start_date, days: 1)
      end_date = Timex.shift(start_date, weeks: 1)

      num_days = Timex.diff(end_date, start_date, :days)

      events =
        Schedule.list_events(
          start_date,
          end_date,
          start_time,
          end_time
        )

      assert Enum.count(events) == num_days * 2
    end

    test "returns all events in the next month for the specified range", context do
      %{creator_1_event: creator_1_event} = context
      {:ok, start_time} = Time.new(0, 0, 0)
      {:ok, end_time} = Time.new(23, 59, 59)

      end_date = Timex.shift(creator_1_event.start_date, months: 1)
      num_days = Timex.diff(creator_1_event.end_date, creator_1_event.start_date, :days) + 1

      events =
        Schedule.list_events(
          creator_1_event.start_date,
          end_date,
          start_time,
          end_time
        )

      assert Enum.count(events) == num_days * 2
    end

    test "when the user_subscription option is provided, it returns all " <>
           "events which that user is subscribed to",
         context do
      %{creator_1_event: creator_1_event, creator_1: creator} = context
      start_date = Timex.shift(creator_1_event.start_date, days: 1)
      end_date = Timex.shift(start_date, weeks: 1)
      user = insert(:user)
      {:ok, _} = Schedule.create_subscription(user.id, creator.id)

      events =
        Schedule.list_events(start_date, end_date, nil, nil, nil, user_subscription: user.id)

      refute Enum.empty?(events)

      Enum.each(events, fn event ->
        assert event.creator_id == creator.id
      end)
    end

    test "when creator_id is provided, it returns only events for that creator", context do
      %{creator_1_event: creator_1_event, creator_1: creator} = context
      start_date = Timex.shift(creator_1_event.start_date, days: 1)
      end_date = Timex.shift(start_date, weeks: 1)

      events = Schedule.list_events(start_date, end_date, nil, nil, nil, creator_id: creator.id)

      refute Enum.empty?(events)

      Enum.each(events, fn event ->
        assert event.creator_id == creator.id
      end)
    end

    @type expected_values :: %{
            required(:expected_start_time) => Timex.DateTime.t(),
            required(:expected_end_time) => Timex.DateTime.t(),
            required(:expected_start_date) => Timex.Date.t(),
            required(:expected_end_date) => Timex.Date.t(),
            required(:expected_platform) => String.t(),
            required(:expected_creator_id) => integer()
          }
    @spec assert_recurring_event(Event.t(), expected_values()) :: nil
    def assert_recurring_event(event, expected_values) do
      %{
        expected_start_time: expected_start_time,
        expected_end_time: expected_end_time,
        expected_start_date: expected_start_date,
        expected_end_date: expected_end_date,
        expected_platform: expected_platform,
        expected_creator_id: expected_creator_id
      } = expected_values

      assert event.start_date == expected_start_date
      assert Timex.equal?(event.start_time, expected_start_time)

      assert event.end_date == expected_end_date
      assert Timex.equal?(event.end_time, expected_end_time)

      assert event.creator_id == expected_creator_id
      assert event.platform == expected_platform
    end
  end
end
