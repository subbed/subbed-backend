defmodule Subbed.ContentTest do
  use Subbed.DataCase
  alias Subbed.{Content, Schedule}
  require Integer

  describe "creators" do
    alias Subbed.Content.Creator

    setup do
      creator = insert(:creator)
      user = insert(:user)
      Schedule.create_subscription(user.id, creator.id)
      creator = Content.get_creator!(creator.id)

      on_exit(fn ->
        # Delete any images that exist in test/support/images
        dir = "test/support/images"

        dir
        |> File.ls!()
        |> Enum.filter(fn s -> not String.starts_with?(s, ".") end)
        |> Enum.map(fn path -> Path.expand(path, "test/support/images") end)
        |> Enum.each(&File.rm_rf!/1)
      end)

      %{creator: creator, user: user}
    end

    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil, timezone: nil}

    test "list_creators/0 returns all creators", %{creator: creator} do
      assert Content.list_creators() == [creator]
    end

    test "list_creators/2 returns all creators that user maintains", %{
      creator: creator,
      user: user
    } do
      Content.add_maintainer(creator.id, user.id)
      assert Content.list_creators(:maintainer, user) == [creator]
    end

    test "list_creators/2 returns all creators that user is subscribed to", %{
      creator: creator,
      user: user
    } do
      assert Content.list_creators(:subscribed_to, user) == [creator]
    end

    test "get_creator!/1 returns the creator with given id", %{creator: creator} do
      assert Content.get_creator!(creator.id) == creator
    end

    test "get_creator!/1 returns the creator with given name", %{creator: creator} do
      assert Content.get_creator_by_name!(creator.name) == creator
    end

    test "create_creator/1 with valid data creates a creator", %{creator: creator} do
      path = "test/support/luc.jpg"
      upload = %{__struct__: Plug.Upload, path: path, filename: Path.basename(path)}
      params = %{timezone: creator.timezone, name: "foo", avatar: upload}

      assert {:ok, %Creator{} = creator} = Content.create_creator(params)
      assert creator.name == params.name
      assert creator.timezone == params.timezone
      assert creator.avatar.file_name == "luc.jpg"

      # A thumbnail and original file should exist in: priv/static/images/creator_avatars/foo
      original_file = "test/support/images/creator_avatars/foo/original.jpg"
      thumbnail_file = "test/support/images/creator_avatars/foo/original.jpg"
      assert File.exists?(original_file)
      assert File.exists?(thumbnail_file)
    end

    test "create_creator/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Content.create_creator(@invalid_attrs)
    end

    test "update_creator/2 with valid data updates the creator", %{creator: creator} do
      assert {:ok, %Creator{} = creator} = Content.update_creator(creator, @update_attrs)
      assert creator.name == "some updated name"
    end

    test "update_creator/2 with invalid data returns error changeset", %{creator: creator} do
      assert {:error, %Ecto.Changeset{}} = Content.update_creator(creator, @invalid_attrs)

      assert creator == Content.get_creator!(creator.id)
    end

    test "update_creator/2 allows you to add an image to an existing creator", %{creator: creator} do
      path = "test/support/luc.jpg"
      upload = %{__struct__: Plug.Upload, path: path, filename: Path.basename(path)}
      params = %{avatar: upload}
      {:ok, creator} = Content.update_creator(creator, params)
      assert creator.avatar.file_name == "luc.jpg"
    end

    test "delete_creator/1 deletes the creator" do
      creator = insert(:creator)
      assert {:ok, %Creator{}} = Content.delete_creator(creator)
      assert_raise Ecto.NoResultsError, fn -> Content.get_creator!(creator.id) end
    end

    test "delete_creator/1 fails to delete a creator that has a subscriber", %{creator: creator} do
      assert_raise Ecto.ConstraintError, fn -> Content.delete_creator(creator) end
    end

    test "change_creator/1 returns a creator changeset", %{creator: creator} do
      assert %Ecto.Changeset{} = Content.change_creator(creator)
    end

    test "add_maintainer/2 adds a maintainer to a creator", %{creator: creator, user: user} do
      # Assert that no maintainers exist
      creator = Repo.preload(creator, :maintainers)
      assert Enum.empty?(creator.maintainers)

      # Assert we correctly creator this maintainer
      {:ok, maintainer} = Content.add_maintainer(creator.id, user.id)
      assert maintainer.creator_id == creator.id
      assert maintainer.user_id == user.id

      # Assert that the creator now has maintainers
      creator = Repo.preload(creator, :maintainers, force: true)
      assert Enum.count(creator.maintainers) == 1
      assert user == List.first(creator.maintainers)
    end

    test "add_maintainer/2 returns an error for an invalid user_id", %{creator: creator} do
      {:error, changeset} = Content.add_maintainer(creator.id, 0)
      refute changeset.valid?
      {reason, _} = changeset.errors[:user_id]
      assert reason == "does not exist"
    end

    test "add_maintainer/2 returns an error for an invalid creator_id", %{user: user} do
      {:error, changeset} = Content.add_maintainer(0, user.id)
      refute changeset.valid?
      {reason, _} = changeset.errors[:creator_id]
      assert reason == "does not exist"
    end

    test "subscription_count returns all unique subscription count for a creator", context do
      %{creator: creator} = context
      creator = get_creator_subscription_count(creator)
      user = insert(:user)
      assert creator.subscription_count == 1

      # Create the media for that creator
      medium_attrs = %{creator: creator}
      medium_1 = insert(:medium, medium_attrs)
      medium_2 = insert(:medium, medium_attrs)

      # Subscribe the user to each of these mediums
      Enum.each([medium_1, medium_2], fn medium ->
        {:ok, _subscription} = Schedule.create_subscription(user.id, creator.id, medium.id)
      end)

      creator = get_creator_subscription_count(creator)
      assert creator.subscription_count == 2
    end

    test "subscription_count returns subscriptions without mediums", context do
      %{creator: creator, user: user} = context
      creator = get_creator_subscription_count(creator)
      assert creator.subscription_count == 1

      Schedule.create_subscription(user.id, creator.id)

      creator = get_creator_subscription_count(creator)
      assert creator.subscription_count == 1
    end

    test "subscription_count raises when a query doesn't have a named binding", context do
      %{creator: creator} = context
      query = from(creator in Content.Creator, where: creator.id == ^creator.id)

      assert_raise RuntimeError, fn ->
        Content.Creator.load_subscription_count(query)
      end
    end

    test "maintainer? returns true if the user is a maintainer", context do
      %{creator: creator, user: user} = context
      {:ok, _} = Content.add_maintainer(creator.id, user.id)
      assert Content.maintainer?(creator.id, user.id)
    end

    test "maintainer? returns false if the user is not a maintainer", context do
      %{creator: creator, user: user} = context
      refute Content.maintainer?(creator.id, user.id)
    end

    defp get_creator_subscription_count(creator) do
      query = from(creator in Content.Creator, as: :creator, where: creator.id == ^creator.id)

      query
      |> Content.Creator.load_subscription_count()
      |> Repo.one()
    end
  end

  describe "media" do
    alias Subbed.Content.Media

    setup do
      medium = insert(:medium)
      %{creator: medium.creator, medium: medium}
    end

    test "list_media/0 returns all media" do
      assert Content.list_media() == Repo.all(Media)
    end

    test "get_medium!/1 returns the medium with given id", %{medium: medium} do
      assert Content.get_medium!(medium.id).id == medium.id
    end

    test "create_medium/1 with valid data creates a medium", %{creator: creator} do
      params = %{name: "some name", type: "podcast", creator_id: creator.id}
      {:ok, %Media{} = medium} = Content.create_medium(params)

      assert medium.name == "some name"
      assert medium.type == :podcast
    end

    test "create_medium/1 with duplicate name from same creator returns error", %{
      creator: creator
    } do
      params = %{name: "some name", type: "podcast", creator_id: creator.id}
      {:ok, %Media{} = _} = Content.create_medium(params)
      {:error, _} = Content.create_medium(params)
    end

    test "create_medium/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Content.create_medium(@invalid_attrs)
    end

    test "update_medium/2 with valid data updates the medium", %{medium: medium, creator: creator} do
      params = %{name: "some updated name", type: "video", creator_id: creator.id}
      assert {:ok, %Media{} = medium} = Content.update_medium(medium, params)
      assert medium.name == "some updated name"
      assert medium.type == :video
    end

    test "update_medium/2 with invalid data returns error changeset", %{
      medium: medium,
      creator: creator
    } do
      params = %{name: nil, type: "porn", creator_id: creator.id}
      assert {:error, %Ecto.Changeset{}} = Content.update_medium(medium, params)
      reloaded_medium = Content.get_medium!(medium.id)
      assert medium.name == reloaded_medium.name
      assert medium.type == reloaded_medium.type
    end

    test "delete_medium/1 deletes the medium", %{medium: medium} do
      assert {:ok, %Media{}} = Content.delete_medium(medium)
      assert_raise Ecto.NoResultsError, fn -> Content.get_medium!(medium.id) end
    end

    test "change_medium/1 returns a medium changeset", %{medium: medium} do
      assert %Ecto.Changeset{} = Content.change_medium(medium)
    end

    test "subscription_count returns the number of subscribers for that medium", %{medium: medium} do
      user = insert(:user)
      medium = get_media_subscription_count(medium)
      assert medium.subscription_count == 0

      {:ok, _subscription} = Schedule.create_subscription(user.id, medium.creator_id, medium.id)

      medium = get_media_subscription_count(medium)
      assert medium.subscription_count == 1
    end

    test "subscription_count raises when a query doesn't have a named binding", %{medium: medium} do
      query = from(medium in Content.Media, where: medium.id == ^medium.id)

      assert_raise RuntimeError, fn ->
        Content.Media.load_subscription_count(query)
      end
    end

    defp get_media_subscription_count(medium) do
      query = from(medium in Content.Media, as: :medium, where: medium.id == ^medium.id)

      query
      |> Content.Media.load_subscription_count()
      |> Repo.one()
    end
  end
end
