defmodule Subbed.AccountTest do
  use Subbed.DataCase

  alias Subbed.Account

  describe "users" do
    alias Subbed.Account.User

    @valid_user_id "user_id"
    @update_attrs %{user_id: "some updated user_id"}
    @invalid_attrs %{user_id: nil}

    def user_fixture() do
      {:ok, user} = Account.create_user(@valid_user_id)
      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Account.list_users() == [user]
    end

    test "get_user/1 returns the user with given id" do
      user = user_fixture()
      assert Account.get_user(user.id) == user
    end

    test "get_user/1 returns nil if the user doesn't exist" do
      assert Account.get_user(0) == nil
    end

    test "get_user_by_user_id returns the user with the given user_id" do
      user = user_fixture()
      assert Account.get_user_by_user_id(user.user_id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Account.create_user(@valid_user_id)
      assert user.user_id == @valid_user_id
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Account.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Account.update_user(user, @update_attrs)
      assert user.user_id == "some updated user_id"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Account.update_user(user, @invalid_attrs)
      assert user == Account.get_user(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Account.delete_user(user)
      assert Account.get_user(user.id) == nil
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Account.change_user(user)
    end
  end
end
