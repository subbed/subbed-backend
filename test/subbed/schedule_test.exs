defmodule Subbed.ScheduleTest do
  use Subbed.DataCase
  require Integer

  alias Subbed.Schedule

  describe "platforms" do
    alias Subbed.Schedule.Platform

    @valid_attrs %{name: "some name", url: "https://twitch.tv"}
    @update_attrs %{name: "some updated name", url: "https://youtube.com"}
    @invalid_attrs %{name: nil, url: nil}

    def platform_fixture(attrs \\ %{}) do
      {:ok, platform} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Schedule.create_platform()

      platform
    end

    test "list_platforms/0 returns all platforms" do
      assert Schedule.list_platforms() == Repo.all(Platform)
    end

    test "get_platform!/1 returns the platform with given id" do
      platform = platform_fixture()
      assert Schedule.get_platform!(platform.id) == platform
    end

    test "create_platform/1 with valid data creates a platform" do
      assert {:ok, %Platform{} = platform} = Schedule.create_platform(@valid_attrs)
      assert platform.name == "some name"
    end

    test "create_platform/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Schedule.create_platform(@invalid_attrs)
    end

    test "update_platform/2 with valid data updates the platform" do
      platform = platform_fixture()
      assert {:ok, %Platform{} = platform} = Schedule.update_platform(platform, @update_attrs)
      assert platform.name == "some updated name"
    end

    test "update_platform/2 with invalid data returns error changeset" do
      platform = platform_fixture()
      assert {:error, %Ecto.Changeset{}} = Schedule.update_platform(platform, @invalid_attrs)
      assert platform == Schedule.get_platform!(platform.id)
    end

    test "delete_platform/1 deletes the platform" do
      platform = platform_fixture()
      assert {:ok, %Platform{}} = Schedule.delete_platform(platform)
      assert_raise Ecto.NoResultsError, fn -> Schedule.get_platform!(platform.id) end
    end

    test "change_platform/1 returns a platform changeset" do
      platform = platform_fixture()
      assert %Ecto.Changeset{} = Schedule.change_platform(platform)
    end
  end

  describe "subscriptions" do
    alias Subbed.Schedule.Subscription

    setup do
      user = insert(:user)
      creator = insert(:creator)
      medium_attrs = %{creator: creator}
      medium1 = insert(:medium, medium_attrs)
      medium2 = insert(:medium, medium_attrs)
      type = "NEW_CONTENT"
      %{creator: creator, user: user, medium1: medium1, medium2: medium2, type: type}
    end

    test "create_subscription/4 with valid data creates a subscription", %{
      creator: creator,
      user: user,
      medium1: medium1,
      type: type
    } do
      {:ok, %Subscription{} = actual_subscription} =
        Schedule.create_subscription(user.id, creator.id, medium1.id, type)

      assert actual_subscription == Schedule.get_subscription!(actual_subscription.id)
    end

    test "create_subscription/4 with subscription to multiple media", %{
      creator: creator,
      user: user,
      medium1: medium1,
      medium2: medium2,
      type: type
    } do
      {:ok, %Subscription{} = actual_subscription1} =
        Schedule.create_subscription(user.id, creator.id, medium1.id, type)

      {:ok, %Subscription{} = actual_subscription2} =
        Schedule.create_subscription(user.id, creator.id, medium2.id, type)

      assert actual_subscription1 == Schedule.get_subscription!(actual_subscription1.id)
      assert actual_subscription2 == Schedule.get_subscription!(actual_subscription2.id)
    end

    test "create_subscription/4 works without a medium", context do
      %{user: user, creator: creator} = context

      {:ok, actual_subscription} = Schedule.create_subscription(user.id, creator.id)

      assert actual_subscription.user_id == user.id
      assert actual_subscription.creator_id == creator.id
      refute actual_subscription.medium_id
      refute actual_subscription.type
    end

    test "errors create_subscription/4 with subscriptions to the same media", %{
      creator: creator,
      user: user,
      medium1: medium1,
      type: type
    } do
      Schedule.create_subscription(user.id, creator.id, medium1.id, type)

      assert_raise(
        Ecto.ConstraintError,
        fn -> Schedule.create_subscription(user.id, creator.id, medium1.id, type) end
      )
    end

    test "create_subscription/4 errors when subscribing to the same creator twice", context do
      %{user: user, creator: creator} = context

      {:ok, _actual_subscription} = Schedule.create_subscription(user.id, creator.id)
      {:error, changeset} = Schedule.create_subscription(user.id, creator.id)

      expected_error =
        {"User (id: #{user.id}) is already subscribed to Creator with ID #{creator.id}",
         [:user_id]}

      refute changeset.valid?
      assert changeset.errors == [creator_id: expected_error]
    end

    test "get_subscription!/1 returns the subscription with given id", %{
      creator: creator,
      user: user
    } do
      {:ok, %Subscription{} = subscription} = Schedule.create_subscription(user.id, creator.id)
      assert Schedule.get_subscription!(subscription.id) == subscription
    end

    test "delete_subscriptions/2 deletes the subscription", %{
      creator: creator,
      user: user,
      medium1: medium1,
      medium2: medium2,
      type: type
    } do
      {:ok, %Subscription{} = _} =
        Schedule.create_subscription(user.id, creator.id, medium1.id, type)

      {:ok, %Subscription{} = _} =
        Schedule.create_subscription(user.id, creator.id, medium2.id, type)

      assert {2, _subs} = Schedule.delete_subscriptions(user, creator)
      user_creators = Repo.preload(user, :creators)
      assert user_creators.creators == []
    end

    test "delete_subscription/1 deletes the subscription by id", %{creator: creator, user: user} do
      {:ok, subscription} = Schedule.create_subscription(user.id, creator.id)
      assert {:ok, %Subscription{}} = Schedule.delete_subscription(subscription.id)
      user_creators = Repo.preload(user, :creators)
      assert user_creators.creators == []
    end
  end
end
