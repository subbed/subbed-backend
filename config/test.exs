use Mix.Config

# Configure your database
config :subbed, Subbed.Repo,
  username: "postgres",
  password: "postgres",
  database: "subbed_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :subbed, SubbedWeb.Endpoint,
  http: [port: 4002],
  server: false,
  client_url: "http://localhost:3000"

# Print only warnings and errors during test
config :logger, level: :warn

config :arc,
  storage: Arc.Storage.Local,
  storage_dir: "test/support/images",
  asset_host: "http://localhost:4000"
