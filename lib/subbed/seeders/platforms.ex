defmodule Subbed.Seeders.Platforms do
  alias Subbed.Schedule
  alias Subbed.Constants

  def seed do
    platforms = [
      %{name: Constants.platforms().twitch, url: "https://twitch.tv"},
      %{name: Constants.platforms().youtube, url: "https://youtube.com"}
    ]

    Enum.each(platforms, fn platform ->
      Schedule.create_platform(platform)
    end)
  end
end
