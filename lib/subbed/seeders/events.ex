defmodule Subbed.Seeders.Events do
  alias Subbed.{Account, Constants, Content, Repo, Schedule}
  import Ecto.Query

  def seed do
    user = Repo.one(from(user in Account.User, order_by: user.id, limit: 1))
    creators = Repo.all(Content.Creator)
    media = Repo.all(Content.Media)

    Enum.each(creators, fn creator ->
      now = Timex.now()
      {:ok, start_time} = now |> Timex.set(hour: 5) |> Schedule.Event.to_time()
      {:ok, end_time} = now |> Timex.shift(hours: 9) |> Schedule.Event.to_time()

      start_date = Timex.to_date(now)
      end_date = Timex.shift(start_date, days: 5)

      params = %Schedule.Event.Params{
        owner_id: user.id,
        creator_id: creator.id,
        medium_id: Enum.random(media).id,
        platform: Constants.platforms().twitch,
        start_date: start_date,
        start_time: start_time,
        end_date: end_date,
        end_time: end_time
      }

      Schedule.create_recurring_event(params, 1, :daily)
    end)
  end
end
