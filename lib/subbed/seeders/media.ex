defmodule Subbed.Seeders.Medium do
  alias Subbed.{Content, Repo}

  def seed do
    creators = Repo.all(Content.Creator)

    Enum.each(creators, fn creator ->
      media = [
        %{name: "Fortnite", type: "stream", creator_id: creator.id},
        %{name: "World of Warcraft", type: "stream", creator_id: creator.id}
      ]

      Enum.each(media, fn medium ->
        Content.create_medium(medium)
      end)
    end)
  end
end
