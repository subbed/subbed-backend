defmodule Subbed.Seeders.Creators do
  alias Subbed.{Account, Constants, Content, Repo, Schedule}
  import Ecto.Query

  defp make_plug_upload(filename) do
    %Plug.Upload{
      content_type: MIME.from_path(filename),
      filename: filename,
      path: Path.expand(filename, "priv/seeder/images")
    }
  end

  defp find_or_create_medium(medium) do
    query =
      from(medium in Content.Media,
        where: medium.name == ^medium.name and medium.creator_id == ^medium.creator_id
      )

    case Repo.one(query) do
      nil ->
        {:ok, medium} = Content.create_medium(medium)
        medium

      medium ->
        medium
    end
  end

  def seed do
    users = Repo.all(Account.User)

    creators = [
      # Streams a bunch of games about every day
      # Starting at 19:30 for ~8.25hours
      %{
        name: "shroud",
        timezone: "America/Los_Angeles",
        avatar: make_plug_upload("shroud.png"),
        events: [
          %{
            medium: %{name: "Random Games", type: "stream"},
            params: %{
              start_date: ~D[2020-02-21],
              end_date: ~D[2021-02-21],
              start_time: ~T[19:30:00],
              end_time: ~T[03:45:00],
              platform: Constants.platforms().twitch
            },
            separation_count: 1,
            recurring_type: :daily,
            recurring_options: []
          }
        ]
      },
      # Streams mainly fornite about 6 days a week (random days off)
      # Starts streaming at 17:20 for about 6.5 hours
      %{
        name: "tfue",
        timezone: "America/Los_Angeles",
        avatar: make_plug_upload("tfue.jpg"),
        events: [
          %{
            medium: %{name: "Fortnite", type: "stream"},
            params: %{
              start_date: ~D[2020-02-21],
              end_date: ~D[2021-02-21],
              start_time: ~T[17:20:00],
              end_time: ~T[00:00:00],
              platform: Constants.platforms().twitch
            },
            separation_count: 1,
            recurring_type: :daily,
            recurring_options: []
          }
        ]
      },
      # Streams EVERY day for an average of 8.6hrs starting at 18:30
      %{
        name: "xqcow",
        timezone: "America/Los_Angeles",
        avatar: make_plug_upload("xqcow.jpeg"),
        events: [
          %{
            medium: %{name: "Random Games", type: "stream"},
            params: %{
              start_date: ~D[2020-02-21],
              end_date: ~D[2021-02-21],
              start_time: ~T[18:30:00],
              end_time: ~T[03:00:00],
              platform: Constants.platforms().twitch
            },
            separation_count: 1,
            recurring_type: :daily,
            recurring_options: []
          }
        ]
      },
      # Streams M, Th, Sat
      # At 1:30PM PST
      # He streams for approximately ~3 hours
      %{
        name: "paymoneywubby",
        timezone: "America/Los_Angeles",
        avatar: make_plug_upload("pay_money_wubby.png"),
        events: [
          # Monday stream
          %{
            medium: %{name: "Just Chatting", type: "stream"},
            params: %{
              start_date: ~D[2020-02-24],
              end_date: ~D[2021-02-24],
              start_time: ~T[13:30:00],
              end_time: ~T[16:30:00],
              platform: Constants.platforms().twitch
            },
            separation_count: 1,
            recurring_type: :weekly,
            recurring_options: [day_of_week: Schedule.day_of_week().monday]
          },
          # Thursday stream
          %{
            medium: %{name: "Just Chatting", type: "stream"},
            params: %{
              start_date: ~D[2020-02-27],
              end_date: ~D[2021-02-27],
              start_time: ~T[13:30:00],
              end_time: ~T[16:30:00],
              platform: Constants.platforms().twitch
            },
            separation_count: 1,
            recurring_type: :weekly,
            recurring_options: [day_of_week: Schedule.day_of_week().thursday]
          },
          # Saturday stream
          %{
            medium: %{name: "Just Chatting", type: "stream"},
            params: %{
              start_date: ~D[2020-02-29],
              end_date: ~D[2021-03-01],
              start_time: ~T[13:30:00],
              end_time: ~T[16:30:00],
              platform: Constants.platforms().twitch
            },
            separation_count: 1,
            recurring_type: :weekly,
            recurring_options: [day_of_week: Schedule.day_of_week().saturday]
          }
        ]
      }
    ]

    Enum.each(creators, fn creator ->
      {events, creator} = Map.pop(creator, :events, [])
      # Create the content creator
      {:ok, creator} = Content.create_creator(creator)

      Enum.each(events, fn event ->
        # Create the medium
        {medium, event} = Map.pop!(event, :medium)

        medium =
          medium
          |> Map.put(:creator_id, creator.id)
          |> find_or_create_medium()

        # Create Recurring event
        owner = Enum.random(users)
        created_params = %{owner_id: owner.id, creator_id: creator.id, medium_id: medium.id}
        params = Map.merge(event.params, created_params)

        {:ok, _event} =
          Schedule.Event.Params
          |> struct(params)
          |> Schedule.create_recurring_event(
            event.separation_count,
            event.recurring_type,
            event.recurring_options
          )
      end)

      # Add all users as maintainers and subscribers
      Enum.each(users, fn user ->
        Content.add_maintainer(creator.id, user.id)
        # Subscribe user to all media by this creator
        Schedule.create_subscription(user.id, creator.id)
      end)
    end)
  end
end
