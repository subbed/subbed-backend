defmodule Subbed.Seeders.Subscriptions do
  import Ecto.Query, warn: false
  alias Subbed.{Account, Content, Repo, Schedule, Constants}

  def seed do
    users = Repo.all(Account.User)
    creators = Repo.all(Content.Creator)

    Enum.each(users, fn user ->
      Enum.each(creators, fn creator ->
        query =
          from m in Content.Media,
            where: m.creator_id == ^creator.id,
            select: m

        media = Repo.all(query)

        Enum.each(media, fn medium ->
          type =
            Constants.subscription_types()
            |> Enum.random()
            |> elem(1)

          Schedule.create_subscription(
            user,
            creator,
            medium,
            type
          )
        end)
      end)
    end)
  end
end
