defmodule Subbed.Seeders.Users do
  alias Subbed.Account

  def seed do
    user_ids = [
      "itsbarakyo@gmail.com",
      "kevin.moore.138@gmail.com",
      "lucmhall@gmail.com",
      "almostanaccount@gmail.com"
    ]

    Enum.each(user_ids, fn user_id ->
      Account.create_user(user_id)
    end)
  end
end
