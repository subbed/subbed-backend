defmodule Subbed.Repo do
  use Ecto.Repo,
    otp_app: :subbed,
    adapter: Ecto.Adapters.Postgres
end
