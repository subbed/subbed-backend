defmodule Subbed.Constants do
  def platforms, do: %{twitch: "Twitch", youtube: "Youtube"}
  def recurring_types, do: %{daily: :daily, weekly: :weekly, monthly: :monthly, yearly: :yearly}
  def medium_types, do: %{podcast: "podcast", show: "show", stream: "stream", video: "video"}
  def subscription_types, do: %{new_content: "NEW_CONTENT", all: "ALL"}
  def event_sort_fields, do: %{start_date: :start_date, inserted_at: :inserted_at}
  def my_creators_filter_fields, do: %{maintainer: :maintainer, subscribed_to: :subscribed_to}
end
