defmodule Subbed.Schedule do
  @moduledoc """
  The Schedule context.
  """

  import Ecto.Query, warn: false
  use Timex
  alias Subbed.Enums
  alias Subbed.Repo
  alias Subbed.Account.User
  alias Subbed.Schedule.Subscription
  alias Subbed.Content.Creator
  alias Subbed.Content.Media

  def data() do
    Dataloader.Ecto.new(Repo, query: &query/2)
  end

  alias Subbed.{Schedule, Schedule.Platform}

  def day_of_week do
    %{
      sunday: 0,
      monday: 1,
      tuesday: 2,
      wednesday: 3,
      thursday: 4,
      friday: 5,
      saturday: 6
    }
  end

  def month_of_year do
    %{
      january: 1,
      february: 2,
      march: 3,
      april: 4,
      may: 5,
      june: 6,
      july: 7,
      august: 8,
      september: 9,
      october: 10,
      november: 11,
      december: 12
    }
  end

  @doc """
  Returns the list of platforms.

  ## Examples

      iex> list_platforms()
      [%Platform{}, ...]

  """
  def list_platforms do
    Repo.all(Platform)
  end

  @doc """
  Gets a single platform.

  Raises `Ecto.NoResultsError` if the Platform does not exist.

  ## Examples

      iex> get_platform!(123)
      %Platform{}

      iex> get_platform!(456)
      ** (Ecto.NoResultsError)

  """
  def get_platform!(id), do: Repo.get!(Platform, id)

  @doc """
  Creates a platform.

  ## Examples

      iex> create_platform(%{field: value})
      {:ok, %Platform{}}

      iex> create_platform(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_platform(attrs \\ %{}) do
    %Platform{}
    |> Platform.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a platform.

  ## Examples

      iex> update_platform(platform, %{field: new_value})
      {:ok, %Platform{}}

      iex> update_platform(platform, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_platform(%Platform{} = platform, attrs) do
    platform
    |> Platform.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Platform.

  ## Examples

      iex> delete_platform(platform)
      {:ok, %Platform{}}

      iex> delete_platform(platform)
      {:error, %Ecto.Changeset{}}

  """
  def delete_platform(%Platform{} = platform) do
    Repo.delete(platform)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking platform changes.

  ## Examples

      iex> change_platform(platform)
      %Ecto.Changeset{source: %Platform{}}

  """
  def change_platform(%Platform{} = platform) do
    Platform.changeset(platform, %{})
  end

  alias Subbed.Schedule.Event

  def generate_dates_and_times_range(date_range) do
    now = Timex.now()

    time_from_now_map = %{
      hour: Timex.shift(now, hours: 1),
      day: Timex.shift(now, days: 1),
      week: Timex.shift(now, weeks: 1),
      month: Timex.shift(now, months: 1)
    }

    time_from_now = time_from_now_map[date_range]

    {:ok, up_to_time} =
      if date_range == :hour do
        Schedule.Event.to_time(time_from_now)
      else
        Time.new(23, 59, 59)
      end

    {up_to_time, Timex.to_date(time_from_now)}
  end

  def list_events(%{}) do
    Repo.all(Event)
  end

  @doc """
  Returns all the events in the given range (inclusive).

  Options:
  * :media_type [atom()] - An optional media type to filter events by.
  * :sort_by [atom()] - Allows you to sort by when an event's start date
    (:start_date) or by when it was created (:inserted_at). Defaults to
    :start_date.
  * :user_subscription [integer()] - when provided will return only those
    events for content that the user is subscribed to.
  * :creator_id [integer()] - When provided will return events only for this
    creator
  """
  @dialyzer {:nowarn_function, {:list_events, 2}}
  @dialyzer {:nowarn_function, {:list_events, 3}}
  @type event_sort_by :: :start_date | :inserted_at
  @type list_event_options :: [
          media_type: Enums.MediaTypeEnum.t(),
          sort_by: event_sort_by(),
          user_subscription: integer(),
          creator_id: integer()
        ]
  @spec list_events(Date.t(), Date.t(), Time.t(), Time.t(), list_event_options()) ::
          list(Event.t())
  def list_events(
        start_date,
        end_date,
        start_time \\ nil,
        end_time \\ nil,
        date_range \\ nil,
        options \\ []
      ) do
    query =
      from(
        event in Event,
        left_join: recurring_pattern in assoc(event, :recurring_pattern),
        where: fragment("? BETWEEN ? AND ?", ^start_date, event.start_date, event.end_date),
        or_where: fragment("? BETWEEN ? AND ?", ^end_date, event.start_date, event.end_date),
        preload: [:recurring_pattern]
      )

    user_subscription = options[:user_subscription]

    query =
      if not is_nil(user_subscription) do
        from(event in query,
          join: creator in assoc(event, :creator),
          join: subscriptions in assoc(creator, :subscriptions),
          where: subscriptions.user_id == ^user_subscription
        )
      else
        query
      end

    query =
      if not is_nil(start_time) do
        from(event in query, where: event.start_time >= ^start_time)
      else
        query
      end

    query =
      if not is_nil(end_time) do
        from(event in query, where: event.end_time <= ^end_time)
      else
        query
      end

    query =
      if not is_nil(date_range) do
        {up_to_time, up_to_date} = generate_dates_and_times_range(date_range)
        {:ok, start_time} = Schedule.Event.to_time(Timex.now())

        from(event in query,
          where:
            event.start_time >= ^start_time and
              event.start_time <= ^up_to_time and
              event.start_date >= ^start_date and event.start_date <= ^up_to_date
        )
      else
        query
      end

    media_type = options[:media_type]

    query =
      if not is_nil(media_type) do
        from(event in query,
          join: medium in assoc(event, :medium),
          where: medium.type == ^media_type
        )
      else
        query
      end

    query =
      if not is_nil(options[:creator_id]) do
        from(event in query, where: event.creator_id == ^options[:creator_id])
      else
        query
      end

    sort_by_field = if options[:sort_by] == :inserted_at, do: :inserted_at, else: :start_date

    query
    |> Repo.all()
    |> Enum.flat_map(fn event ->
      if not is_nil(date_range) do
        [event]
      else
        Schedule.Event.generate_recurring_events(event, start_date, end_date)
      end
    end)
    |> sort_events(sort_by_field)
  end

  defp sort_events(events, :inserted_at) do
    Enum.sort(events, fn a, b ->
      Timex.before?(a.inserted_at, b.inserted_at)
    end)
  end

  defp sort_events(events, _) do
    Enum.sort(events, fn a, b ->
      a_datetime = Schedule.Event.to_datetime(a.start_date, a.start_time)
      b_datetime = Schedule.Event.to_datetime(b.start_date, b.start_time)
      Timex.before?(a_datetime, b_datetime)
    end)
  end

  @doc """
  Gets a single event.

  Raises `Ecto.NoResultsError` if the Event does not exist.

  ## Examples

      iex> get_event!(123)
      %Event{}

      iex> get_event!(456)
      ** (Ecto.NoResultsError)

  """
  def get_event!(id), do: Repo.get!(Event, id)

  @doc """
  Creates a event.

  ## Examples

      iex> create_event(%{field: value})
      {:ok, %Event{}}

      iex> create_event(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_event(attrs \\ %{}) do
    %Event{}
    |> Event.changeset(attrs)
    |> Repo.insert()
  end

  @type recurring_event_options :: [
          day_of_week: integer(),
          day_of_month: integer(),
          week_of_month: integer(),
          month_of_year: integer()
        ]

  @doc """
  Creates an `Event.t()` and its `RecurringPattern.t()` child.

  Arguments:
  * `params` - A struct of type `Schedule.Event.Params.t()` these are the params
  for creating the event.

  * `separation_count` - The number of `Enums.RecurryingTypeEnum.t()` between
  the recurring events. Ex: If `recurring_type` is `:daily` and
  `separation_count` is `1`, this event will occur every day.

  * `recurring_type` - `:daily`, `:monthly`, `:yearly`

  * `options` - A Keyword list providing information on how this event recurrs.
  Note: Not all options are valid at once. Certain fields are only valid based
  on the provided `recurring_type`.

  Recurring Type:
  * `:daily`: No options are needed nor valid.
  * `:weekly`: Only `:day_of_week` is a valid option.
    * Ex: I want a recurring event to happen 2 weeks on Tuesday.
  * `:monthly`:
    * `:day_of_month`: Every 2 months on the 15th
    * [`:week_of_month`, `:day_of_week`]: Every 4th thursday of the month
  * `:yearly`:
  * [`:day_of_month`, `:month_of_year`]: The 25th of December (Xmas)
  * [`:week_of_month`, `:day_of_week`, `:month_of_year`]: Every 4th Thursday of
  November (Thanksgiving)

  """
  @spec create_recurring_event(
          Schedule.Event.Params.t(),
          integer(),
          Enums.RecurryingTypeEnum.t(),
          recurring_event_options()
        ) :: {:ok, Schedule.Event.t()} | {:error, Ecto.Changeset.t()}
  def create_recurring_event(params, separation_count, recurring_type, options \\ []) do
    recurring_pattern =
      Enum.into(options, %{separation_count: separation_count, recurring_type: recurring_type})

    params = Map.put(params, :recurring_pattern, recurring_pattern)

    %Event{}
    |> Event.recurring_event_changeset(params)
    |> Repo.insert()
  end

  @doc """
  Updates a event.

  ## Examples

      iex> update_event(event, %{field: new_value})
      {:ok, %Event{}}

      iex> update_event(event, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_event(%Event{} = event, attrs) do
    event
    |> Event.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Event.

  ## Examples

      iex> delete_event(event)
      {:ok, %Event{}}

      iex> delete_event(event)
      {:error, %Ecto.Changeset{}}

  """
  def delete_event(%Event{} = event) do
    Repo.delete(event)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking event changes.

  ## Examples

      iex> change_event(event)
      %Ecto.Changeset{source: %Event{}}

  """
  def change_event(%Event{} = event) do
    Event.changeset(event, %{})
  end

  @doc """
  Creates a subscription.

  ## Examples

      iex> create_subscription(user = %User{}, creator = %Creator{}, media = %Media{}, type)
      {:ok, %Subscription{}}

      iex> create_subscription(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_subscription(
        user_id,
        creator_id,
        medium_id \\ nil,
        type \\ nil
      ) do
    params = %{user_id: user_id, creator_id: creator_id, medium_id: medium_id, type: type}

    try do
      %Subscription{}
      |> Subscription.changeset(params)
      |> Repo.insert()
    rescue
      error in Postgrex.Error ->
        case error.postgres.code do
          :integrity_constraint_violation ->
            # An integrity_constraint_violation means that this insert failed
            # the validate_unique_subscription_trigger and they're already
            # subscribed to this creator.
            changeset =
              %Subscription{}
              |> Subscription.changeset(params)
              |> Ecto.Changeset.add_error(:creator_id, error.postgres.message, [:user_id])

            {:error, changeset}

          _ ->
            # If there isn't a match, re-raise the error
            raise error
        end
    end
  end

  @doc """
  Gets a single subscription.

  Raises `Ecto.NoResultsError` if the Subscription does not exist.

  ## Examples

      iex> get_subscription!(123)
      %Subscription{}

      iex> get_subscription!(456)
      ** (Ecto.NoResultsError)

  """
  def get_subscription!(id), do: Repo.get!(Subscription, id)

  @doc """
  Deletes a Subscription.

  ## Examples

      iex> delete_subscription(user, creator)
      {:ok, %Subscription{}}

      iex> delete_subscription(user, creator)
      {:error, %Ecto.Changeset{}}

  """
  def delete_subscriptions(user = %User{}, creator = %Creator{}) do
    query =
      from(
        subscription in Subscription,
        where: subscription.user_id == ^user.id,
        where: subscription.creator_id == ^creator.id,
        select: subscription
      )

    Repo.delete_all(query)
  end

  @doc """
  Deletes a Subscription.

  ## Examples

      iex> delete_subscription(123)
      {:ok, %Subscription{}}

      iex> delete_subscription(123
      {:error, %Ecto.Changeset{}}

  """
  def delete_subscription(id) do
    query =
      from(
        subscription in Subscription,
        where: subscription.id == ^id
      )

    query
    |> Repo.one()
    |> Repo.delete()
  end

  @doc """
  Deletes a Subscription.

  ## Examples

      iex> delete_subscription(subscription)
      {:ok, %Subscription{}}

      iex> delete_subscription(subscription)
      {:error, %Ecto.Changeset{}}

  """
  def delete_subscription(user = %User{}, media = %Media{}) do
    query =
      from(
        subscription in Subscription,
        where: subscription.user_id == ^user.id,
        where: subscription.medium_id == ^media.id
      )

    query
    |> Repo.one()
    |> Repo.delete()
  end

  def query(Creator, _) do
    Creator
    |> Subscription.load_subscription_count()
  end

  def query(queryable, _params) do
    queryable
  end
end
