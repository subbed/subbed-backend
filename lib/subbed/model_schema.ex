defmodule Subbed.ModelSchema do
  @moduledoc """
  Defines a set of use statements, imports, and configurations that are
  consistent across all schemas. Please use this instead of `Ecto.Schema`.

  We mainly want to use this because our timestamps are stored as timestamps
  with time zone. To make sure you don't forget that or have any issues when
  adding data, you'll want to make sure you use this module instead of the
  `Ecto.Schema`.
  """
  defmacro __using__(_opts) do
    quote do
      use Ecto.Schema
      import Ecto.Changeset
      @timestamps_opts [type: :utc_datetime]
    end
  end
end
