defmodule Subbed.Account.User do
  use Subbed.ModelSchema
  alias Subbed.{Content, Schedule}

  schema "users" do
    field(:user_id, :string)

    many_to_many(:creators, Content.Creator,
      join_through: Schedule.Subscription,
      on_replace: :delete
    )

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:user_id])
    |> validate_required([:user_id])
    |> unique_constraint(:user_id)
  end
end
