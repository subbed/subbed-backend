defmodule Subbed.Content do
  alias Subbed.{Content, Repo, Schedule}
  import Ecto.Query

  @doc """
  Returns the list of creators.

  ## Examples

      iex> list_creators()
      [%Content.Creator{}, ...]

  """
  def list_creators() do
    Content.Creator
    |> Schedule.Subscription.load_subscription_count()
    |> Repo.all()
  end

  def list_creators(:maintainer, user) do
    query =
      from(
        creator in Content.Creator,
        inner_join: maintainer in Content.Maintainer,
        on: maintainer.user_id == ^user.id and maintainer.creator_id == creator.id
      )

    query
    |> Schedule.Subscription.load_subscription_count()
    |> Repo.all()
  end

  def list_creators(:subscribed_to, user) do
    query =
      from(
        creator in Content.Creator,
        inner_join: subscription in Schedule.Subscription,
        on: subscription.user_id == ^user.id and subscription.creator_id == creator.id
      )

    query
    |> Schedule.Subscription.load_subscription_count()
    |> Repo.all()
  end

  def get_creator(id) do
    Content.Creator
    |> Schedule.Subscription.load_subscription_count()
    |> Repo.get(id)
  end

  @doc """
  Gets a single creator and loads subscription count.

  Raises `Ecto.NoResultsError` if the Content.Creator does not exist.

  ## Examples

      iex> get_creator!(123)
      %Content.Creator{}

      iex> get_creator!(456)
      ** (Ecto.NoResultsError)

  """
  def get_creator!(id) do
    Content.Creator
    |> Schedule.Subscription.load_subscription_count()
    |> Repo.get!(id)
  end

  @doc """
  Gets a single creator by name and loads subscription count.

  Raises `Ecto.NoResultsError` if the Content.Creator does not exist.

  ## Examples

      iex> get_creator!("name")
      %Content.Creator{}

      iex> get_creator!("name")
      ** (Ecto.NoResultsError)

  """
  def get_creator_by_name!(name) do
    Content.Creator
    |> Schedule.Subscription.load_subscription_count()
    |> Repo.get_by!(name: name)
  end

  @doc """
  Creates a creator.

  ## Examples

      iex> create_creator(%{field: value})
      {:ok, %Content.Creator{}}

      iex> create_creator(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_creator(attrs \\ %{}) do
    %Content.Creator{}
    |> Content.Creator.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a creator.

  ## Examples

      iex> update_creator(creator, %{field: new_value})
      {:ok, %Content.Creator{}}

      iex> update_creator(creator, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_creator(%Content.Creator{} = creator, attrs) do
    creator
    |> Content.Creator.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Content.Creator.

  ## Examples

      iex> delete_creator(creator)
      {:ok, %Content.Creator{}}

      iex> delete_creator(creator)
      {:error, %Ecto.Changeset{}}

  """
  def delete_creator(%Content.Creator{} = creator) do
    Repo.delete(creator)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking creator changes.

  ## Examples

      iex> change_creator(creator)
      %Ecto.Changeset{source: %Content.Creator{}}

  """
  def change_creator(%Content.Creator{} = creator) do
    Content.Creator.changeset(creator, %{})
  end

  @spec add_maintainer(integer(), integer()) ::
          {:ok, Content.Maintainer.t()} | {:error, Ecto.Changeset.t()}
  def add_maintainer(creator_id, user_id) do
    %Content.Maintainer{}
    |> Content.Maintainer.changeset(creator_id, user_id)
    |> Repo.insert()
  end

  @spec maintainer?(integer(), integer()) :: boolean()
  def maintainer?(creator_id, user_id) do
    query =
      from(maintainer in Content.Maintainer,
        where: maintainer.creator_id == ^creator_id,
        where: maintainer.user_id == ^user_id
      )

    Repo.exists?(query)
  end

  alias Subbed.Content.Media

  @doc """
  Returns the list of media.

  ## Examples

      iex> list_media()
      [%Medium{}, ...]

  """
  def list_media do
    Repo.all(Media)
  end

  @doc """
  Gets a single medium.

  Raises `Ecto.NoResultsError` if the Media does not exist.

  ## Examples

      iex> get_medium!(123)
      %Media{}

      iex> get_medium!(456)
      ** (Ecto.NoResultsError)

  """
  def get_medium!(id), do: Repo.get!(Media, id)

  @doc """
  Creates a medium.

  ## Examples

      iex> create_medium(%{field: value})
      {:ok, %Media{}}

      iex> create_medium(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_medium(attrs \\ %{}) do
    %Media{}
    |> Media.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a medium.

  ## Examples

      iex> update_medium(medium, %{field: new_value})
      {:ok, %Media{}}

      iex> update_medium(medium, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_medium(%Media{} = medium, attrs) do
    medium
    |> Media.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Media.

  ## Examples

      iex> delete_medium(medium)
      {:ok, %Media{}}

      iex> delete_medium(medium)
      {:error, %Ecto.Changeset{}}

  """
  def delete_medium(%Media{} = medium) do
    Repo.delete(medium)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking medium changes.

  ## Examples

      iex> change_medium(medium)
      %Ecto.Changeset{source: %Media{}}

  """
  def change_medium(%Media{} = medium) do
    Media.changeset(medium, %{})
  end
end
