defmodule Subbed.Schedule.Event do
  use Subbed.ModelSchema
  alias Subbed.{Account, Content, Schedule, Schedule.Event, Repo}

  @type t() :: %Event{}

  schema "events" do
    field(:name, :string, default: "")
    field(:description, :string, default: "")
    field(:start_date, :date)
    field(:start_time, :time)
    field(:end_time, :time)
    field(:end_date, :date)
    field(:platform, :string)
    field(:is_full_day_event, :boolean, default: false)
    field(:is_recurring, :boolean, default: false)

    belongs_to(:owner, Account.User)
    belongs_to(:creator, Content.Creator)
    belongs_to(:parent_event, Schedule.Event)
    belongs_to(:medium, Content.Media)

    has_one(:recurring_pattern, Schedule.RecurringPattern)

    timestamps()
  end

  defmodule Params do
    @enforce_keys [
      :owner_id,
      :creator_id,
      :medium_id,
      :platform,
      :end_time,
      :end_date,
      :start_time,
      :start_date
    ]
    defstruct [
      :owner_id,
      :creator_id,
      :medium_id,
      :platform,
      :end_time,
      :end_date,
      :start_time,
      :start_date
    ]
  end

  @doc false
  def changeset(event, attrs) do
    event
    |> cast(attrs, [
      :name,
      :description,
      :start_date,
      :start_time,
      :end_time,
      :end_date,
      :owner_id,
      :creator_id,
      :platform,
      :is_full_day_event,
      :is_recurring,
      :parent_event_id,
      :medium_id
    ])
    |> validate_required([
      :start_date,
      :start_time,
      :end_time,
      :end_date,
      :owner_id,
      :creator_id,
      :medium_id,
      :platform
    ])
    |> validate_required([
      :start_time,
      :end_time,
      :owner_id,
      :creator_id,
      :medium_id,
      :platform
    ])
    |> foreign_key_constraint(:platform)
    |> assoc_constraint(:owner)
    |> assoc_constraint(:creator)
    |> assoc_constraint(:parent_event)
    |> assoc_constraint(:medium)
  end

  @spec recurring_event_changeset(Event.t(), Event.Params.t()) :: Ecto.Changeset.t()
  def recurring_event_changeset(event, attrs) do
    attrs = Map.from_struct(attrs)

    event
    |> Repo.preload(:recurring_pattern)
    |> changeset(attrs)
    |> put_change(:is_recurring, true)
    |> cast_assoc(:recurring_pattern)
  end

  @doc """
  Generates the next set of events from the `event.start_date` to `end_date`.
  """
  @spec generate_recurring_events(Event.t(), Date.t(), Date.t()) :: list(Event.t())
  def generate_recurring_events(%Event{recurring_pattern: nil} = event, _, _),
    do: [event]

  def generate_recurring_events(%Event{} = event, start_date, end_date),
    do: generate_recurring_events(event, start_date, end_date, nil, [])

  defp generate_recurring_events(
         event,
         start_date,
         end_date,
         last_generated_event,
         acc
       ) do
    next_event =
      if is_nil(last_generated_event) do
        shift_event(event, true)
      else
        shift_event(last_generated_event, false)
      end

    event_placement = get_event_placement(next_event, event, start_date, end_date)

    case event_placement do
      :inside_range ->
        # inside_range? and before_event_end_date? ->
        generate_recurring_events(event, start_date, end_date, next_event, [next_event | acc])

      :before_end_date ->
        # before_end_date? ->
        generate_recurring_events(event, start_date, end_date, next_event, acc)

      :outside_range ->
        # The next generated event falls outside of the end date or the included
        # range. This means we've hit the last event, so we'll just return the
        # generated events.
        acc
    end
  end

  defp get_event_placement(next_event, event, start_date, end_date) do
    to_event_end_date = Timex.compare(next_event.start_date, event.end_date)
    to_end_date = Timex.compare(next_event.start_date, end_date)
    to_start_date = Timex.compare(next_event.start_date, start_date)

    cond do
      to_start_date >= 0 and to_end_date <= 0 and to_event_end_date <= 0 ->
        :inside_range

      to_end_date <= 0 and to_event_end_date <= 0 ->
        :before_end_date

      true ->
        :outside_range
    end
  end

  @spec shift_event(Event.t(), boolean()) :: Event.t()
  defp shift_event(event, first_event?) do
    start_date =
      if first_event?,
        do: event.start_date,
        else: Schedule.RecurringPattern.shift_event(event, event.recurring_pattern)

    end_date =
      if Timex.before?(event.end_time, event.start_time),
        do: Timex.shift(start_date, days: 1),
        else: start_date

    event
    |> Map.put(:start_date, start_date)
    |> Map.put(:end_date, end_date)
    |> Map.delete(:id)
  end

  @doc """
  Returns `datetime` in the format of HH:MM:SS where HH is in 24 hours.
  """
  @spec to_time(Datetime.t()) :: {:ok, Time.t()} | {:error, atom()}
  def to_time(datetime) do
    %{hour: hour, minute: minute, second: second} = datetime
    Time.new(hour, minute, second)
  end

  @doc """
  Returns `datetime` in the format of HH:MM:SS where HH is in 24 hours.
  Raises if invalid.
  """
  @spec to_time!(Datetime.t()) :: Time.t()
  def to_time!(datetime) do
    %{hour: hour, minute: minute, second: second} = datetime

    case Time.new(hour, minute, second) do
      {:ok, time} ->
        time

      {:error, err} ->
        raise ArgumentError, err
    end
  end

  @doc """
  Convers the given `date` and `time` into `DateTime.t()` struct.
  """
  @spec to_datetime(Date.t(), Time.t()) :: DateTime.t()
  def to_datetime(%Date{} = date, %Time{} = time) do
    %{year: year, month: month, day: day} = date
    %{hour: hour, minute: minute, second: second} = time
    Timex.to_datetime({{year, month, day}, {hour, minute, second}}, "UTC")
  end
end
