defmodule Subbed.Schedule.Platform do
  use Subbed.ModelSchema

  schema "platforms" do
    field(:name, :string)
    field(:url, :string)

    timestamps()
  end

  @doc false
  def changeset(platform, attrs) do
    platform
    |> cast(attrs, [:name, :url])
    |> validate_required([:name, :url])
    |> unique_constraint(:name)
  end
end
