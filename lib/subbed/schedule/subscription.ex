defmodule Subbed.Schedule.Subscription do
  use Subbed.ModelSchema
  import Ecto.Query
  alias Subbed.{Account, Content, Schedule.Subscription, Enums}

  @already_exists "ALREADY_EXISTS"

  @type t() :: %Subscription{}

  schema "subscriptions" do
    field(:type, Enums.SubscriptionTypeEnum)

    belongs_to(:user, Account.User, primary_key: false)
    belongs_to(:creator, Content.Creator, primary_key: false)
    belongs_to(:medium, Content.Media, primary_key: false)

    timestamps()
  end

  @doc false
  def changeset(subscription, attrs) do
    subscription
    |> cast(attrs, [:user_id, :creator_id, :medium_id, :type])
    |> validate_required([:user_id, :creator_id])
    |> foreign_key_constraint(:creator_id)
    |> foreign_key_constraint(:user_id)
    |> foreign_key_constraint(:medium_id)
    |> unique_constraint(:user_creator,
      name: :user_id_creator_id_unique_index,
      message: @already_exists
    )
    |> unique_constraint(:creator_media,
      name: :creator_id_medium_id_unique_index,
      message: @already_exists
    )
  end

  def load_subscription_count(query) do
    from(
      s in query,
      left_join: sub in Subscription,
      on: sub.creator_id == s.id,
      group_by: s.id,
      select_merge: %{subscription_count: count(sub.id)}
    )
  end
end
