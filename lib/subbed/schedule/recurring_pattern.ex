defmodule Subbed.Schedule.RecurringPattern do
  use Subbed.ModelSchema
  alias Subbed.{Enums, Schedule, Schedule.RecurringPattern}

  schema "recurring_patterns" do
    field(:recurring_type, Enums.RecurringTypeEnum)
    field(:day_of_month, :integer)
    field(:day_of_week, :integer)
    field(:month_of_year, :integer)
    field(:separation_count, :integer, default: 0)
    field(:week_of_month, :integer)
    belongs_to(:event, Schedule.Event)
  end

  @doc false
  def changeset(recurring_pattern, attrs) do
    recurring_pattern
    |> cast(attrs, [
      :recurring_type,
      :separation_count,
      :day_of_week,
      :week_of_month,
      :day_of_month,
      :month_of_year
    ])
    |> validate_required([
      :recurring_type,
      :separation_count
    ])
    |> validate_correct_params()
    |> assoc_constraint(:event)
  end

  defmodule RecurringParams do
    @enforce_keys [
      :recurring_type,
      :day_of_week,
      :week_of_month,
      :day_of_month,
      :month_of_year
    ]

    defstruct [
      :recurring_type,
      :day_of_week,
      :week_of_month,
      :day_of_month,
      :month_of_year
    ]
  end

  @spec get_recurring_unit(Enums.RecurringTypeEnum.t()) :: :days | :months | :years
  def get_recurring_unit(recurring_type) do
    case recurring_type do
      :daily ->
        :days

      :weekly ->
        :weeks

      :monthly ->
        :months

      :yearly ->
        :years
    end
  end

  def shift_event(event, %RecurringPattern{
        recurring_type: :daily,
        separation_count: separation_count
      }),
      do: Timex.shift(event.start_date, days: separation_count)

  def shift_event(event, %RecurringPattern{
        recurring_type: :weekly,
        day_of_week: day_of_week,
        separation_count: separation_count
      }) do
    day_shift = weekday_shift(event.start_date, day_of_week)
    Timex.shift(event.start_date, days: day_shift, weeks: separation_count)
  end

  def shift_event(event, %RecurringPattern{
        recurring_type: :monthly,
        day_of_month: day_of_month,
        separation_count: separation_count
      })
      when not is_nil(day_of_month) do
    event.start_date
    |> Timex.shift(months: separation_count)
    |> Timex.set(day: day_of_month)
  end

  def shift_event(event, %RecurringPattern{
        recurring_type: :monthly,
        day_of_week: day_of_week,
        week_of_month: week_of_month,
        separation_count: separation_count
      }) do
    # When doing multiple operations, first shift by the largest one (in this case months)
    event_date = Timex.shift(event.start_date, months: separation_count)
    day_shift = weekday_shift(event_date, day_of_week)
    week_of_month_shift = month_week_shift(event_date, week_of_month, day_of_week)

    shift_options = [days: day_shift, weeks: week_of_month_shift]
    Timex.shift(event_date, shift_options)
  end

  def shift_event(event, %RecurringPattern{
        recurring_type: :yearly,
        day_of_month: day_of_month,
        separation_count: separation_count
      })
      when not is_nil(day_of_month) do
    event.start_date
    |> Timex.shift(years: separation_count)
    |> Timex.set(day: day_of_month)
  end

  def shift_event(event, %RecurringPattern{
        recurring_type: :yearly,
        day_of_week: day_of_week,
        month_of_year: month_of_year,
        week_of_month: week_of_month,
        separation_count: separation_count
      }) do
    event_date =
      event.start_date
      |> Timex.shift(years: separation_count)
      |> Timex.set(month: month_of_year)

    day_shift = weekday_shift(event_date, day_of_week)
    week_of_month_shift = month_week_shift(event_date, week_of_month, day_of_week)

    Timex.shift(event_date, days: day_shift, weeks: week_of_month_shift)
  end

  defp weekday_shift(event_date, day_of_week) do
    weekday = Timex.weekday(event_date)
    week_day_diff = abs(day_of_week - weekday)
    week_day_diff * shift_direction(day_of_week, weekday)
  end

  defp month_week_shift(event_date, week_of_month, target_weekday) do
    first_of_month = Timex.beginning_of_month(event_date)
    first_of_month_weekday = Timex.weekday(first_of_month)

    month_week = Timex.week_of_month(event_date)
    week_of_month_diff = abs(month_week - week_of_month)
    shift_direction = shift_direction(week_of_month, month_week)

    if target_weekday < first_of_month_weekday do
      week_of_month_diff * shift_direction + 1
    else
      week_of_month_diff * shift_direction
    end
  end

  defp shift_direction(target, current) do
    if target < current, do: -1, else: 1
  end

  defp validate_correct_params(%Ecto.Changeset{valid?: false} = changeset), do: changeset

  defp validate_correct_params(%Ecto.Changeset{valid?: true} = changeset) do
    recurring_type = get_field(changeset, :recurring_type)
    day_of_month = get_field(changeset, :day_of_month)
    day_of_week = get_field(changeset, :day_of_week)
    week_of_month = get_field(changeset, :week_of_month)
    month_of_year = get_field(changeset, :month_of_year)

    recurring_params = %RecurringParams{
      recurring_type: recurring_type,
      day_of_week: day_of_week,
      week_of_month: week_of_month,
      day_of_month: day_of_month,
      month_of_year: month_of_year
    }

    if is_valid_recurring_params?(recurring_params) do
      changeset
    else
      Ecto.Changeset.add_error(
        changeset,
        :recurring_type,
        "Invalid parameters provided for the recurring type."
      )
    end
  end

  defp is_valid_recurring_params?(%RecurringParams{
         recurring_type: :daily,
         day_of_week: nil,
         week_of_month: nil,
         day_of_month: nil,
         month_of_year: nil
       }),
       do: true

  defp is_valid_recurring_params?(%RecurringParams{
         recurring_type: :weekly,
         day_of_week: dow,
         week_of_month: nil,
         day_of_month: nil,
         month_of_year: nil
       })
       when dow > 0 and dow <= 7,
       do: true

  defp is_valid_recurring_params?(%RecurringParams{
         recurring_type: :monthly,
         day_of_week: nil,
         day_of_month: dom,
         week_of_month: nil,
         month_of_year: nil
       })
       when dom > 0 and dom <= 31,
       do: true

  defp is_valid_recurring_params?(%RecurringParams{
         recurring_type: :monthly,
         day_of_week: dow,
         day_of_month: nil,
         week_of_month: wom,
         month_of_year: nil
       })
       when wom > 0 and wom <= 4 and dow > 0 and dow <= 7,
       do: true

  defp is_valid_recurring_params?(%RecurringParams{
         recurring_type: :yearly,
         day_of_week: nil,
         day_of_month: dom,
         week_of_month: nil,
         month_of_year: moy
       })
       when dom > 0 and dom <= 31 and moy > 0 and moy <= 12,
       do: true

  defp is_valid_recurring_params?(%RecurringParams{
         recurring_type: :yearly,
         day_of_week: dow,
         day_of_month: nil,
         week_of_month: wom,
         month_of_year: moy
       })
       when wom > 0 and wom <= 4 and moy > 0 and moy <= 12 and dow > 0 and dow <= 7,
       do: true

  defp is_valid_recurring_params?(_), do: false
end
