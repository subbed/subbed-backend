defmodule Subbed.Enums do
  import EctoEnum
  alias Subbed.Constants

  defenum(RecurringTypeEnum, :recurring_type, [
    Constants.recurring_types().daily,
    Constants.recurring_types().weekly,
    Constants.recurring_types().monthly,
    Constants.recurring_types().yearly
  ])

  defenum(MediaTypeEnum, :media_type, [
    Constants.medium_types().podcast,
    Constants.medium_types().show,
    Constants.medium_types().stream,
    Constants.medium_types().video
  ])

  defenum(SubscriptionTypeEnum, :subscription_type, [
    Constants.subscription_types().new_content,
    Constants.subscription_types().all
  ])
end
