defmodule Subbed.Content.Media do
  use Subbed.ModelSchema
  alias Subbed.{Enums, Content, Schedule}
  import Ecto.Query

  @already_exists "ALREADY_EXISTS"

  schema "media" do
    field :name, :string
    field :type, Enums.MediaTypeEnum
    field(:subscription_count, :integer, virtual: true)

    belongs_to :creator, Content.Creator
    has_many(:subscriptions, Schedule.Subscription, foreign_key: :medium_id)

    timestamps()
  end

  @doc false
  def changeset(medium, attrs) do
    medium
    |> cast(attrs, [:name, :type, :creator_id])
    |> validate_required([:name, :type, :creator_id])
    |> unique_constraint(:name,
      name: :media_name_creator_id_index,
      message: @already_exists
    )
  end

  @doc """
  Loads the subscription count for the medium defined in the `query`.
  Note: Your query must have a named binding of `:medium` to be able to use
  this function without getting an error.
  To create a named binding you can do something like:

  ```
  from(medium in Content.Media, as: :medium)
  ```

  This will ensure that we don't try to access `medium` in a query
  that doesn't have it.
  """
  @spec load_subscription_count(Ecto.Query.t()) :: Ecto.Query.t()
  def load_subscription_count(query) do
    if not has_named_binding?(query, :medium) do
      raise "Missing named binding :medium. Ensure your query has :medium " <>
              "before loading subscription_count"
    else
      from(
        [medium: medium] in query,
        left_join: subscriptions in assoc(medium, :subscriptions),
        group_by: medium.id,
        select_merge: %{subscription_count: count(subscriptions.id)}
      )
    end
  end
end
