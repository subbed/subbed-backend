defmodule Subbed.Content.Creator do
  use Subbed.ModelSchema
  use Arc.Ecto.Schema
  import Ecto.Query
  alias Subbed.{Account, Schedule}

  schema "creators" do
    field(:name, :string)
    field(:timezone, :string)
    field(:subscription_count, :integer, virtual: true)
    field(:avatar, SubbedWeb.Uploaders.Avatar.Type)

    has_many(:subscriptions, Schedule.Subscription)

    many_to_many(
      :maintainers,
      Account.User,
      join_through: "maintainers",
      on_replace: :delete
    )

    timestamps()
  end

  @doc false
  def changeset(creator, attrs) do
    creator
    |> cast(attrs, [:name, :timezone])
    |> cast_attachments(attrs, [:avatar])
    |> validate_required([:name, :timezone])
    |> unique_constraint(:name)
    |> foreign_key_constraint(:timezone)
  end

  @doc """
  Loads the subscription count for `creator` in `query`. The subscription count
  for a creator is unique per user. This means that if a creator has multiple
  media (ex: Fortnite and WoW) and the user is subscribed to both, they will
  only count as a single subscriber.

  This function requires that your query provide a named binding of :creator.
  To provide a named binding, you can do something like:
  ```
  from(
    creator in Content.Creator,
    as: :creator,
    where: creator.id == ^creator.id
  )
  ```

  Returns the query with the loaded subscription_count field.
  """
  @spec load_subscription_count(Ecto.Query.t()) :: Ecto.Query.t()
  def load_subscription_count(query) do
    if not has_named_binding?(query, :creator) do
      raise "Missing named binding :creator. Ensure your query has :creator " <>
              "before loading subscription_count"
    else
      from(
        [creator: creator] in query,
        select_merge: %{
          creator
          | subscription_count:
              fragment(
                """
                SELECT count(subscribers.user_id)
                FROM (
                  SELECT user_id
                  FROM subscriptions
                  WHERE creator_id = ?
                  GROUP BY subscriptions.user_id
                ) subscribers
                """,
                creator.id
              )
        }
      )
    end
  end
end
