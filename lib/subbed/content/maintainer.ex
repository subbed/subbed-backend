defmodule Subbed.Content.Maintainer do
  use Subbed.ModelSchema
  alias Subbed.{Account, Content}

  schema "maintainers" do
    belongs_to(:creator, Content.Creator)
    belongs_to(:user, Account.User)

    timestamps()
  end

  @spec changeset(Maintainer.t(), integer(), integer()) ::
          {:ok, Ecto.Changeset.t()} | {:error, Ecto.Changeset.t()}
  def changeset(maintainer, creator_id, user_id) do
    attrs = %{creator_id: creator_id, user_id: user_id}

    maintainer
    |> cast(attrs, [:user_id, :creator_id])
    |> validate_required([:user_id, :creator_id])
    |> foreign_key_constraint(:creator_id)
    |> foreign_key_constraint(:user_id)
    |> unique_constraint(:user_creator,
      name: :user_id_creator_id_unique_index
    )
  end
end
