defmodule SubbedWeb.Uploaders.Avatar do
  use Arc.Definition

  # Include ecto support
  use Arc.Ecto.Definition

  # Original and thumbnail versions:
  @versions [:original, :thumbnail]

  # Whitelist file extensions:
  def validate({file, _}) do
    ~w(.jpg .jpeg .gif .png) |> Enum.member?(Path.extname(file.file_name))
  end

  # Define a thumbnail transformation:
  def transform(:thumbnail, _) do
    {:convert,
     "-strip -thumbnail 200x200^ -gravity center -extent 200x200 -format jpg -limit area 10MB -limit disk 100MB",
     :jpg}
  end

  def filename(version, {_file, scope}) do
    %{name: name} = scope
    version_string = Atom.to_string(version)
    kebab_name = Recase.to_kebab(name)

    # Path to file should be something like: "creator_avatars/foo/thumb.jpg"
    "creator_avatars/#{kebab_name}/#{version_string}"
  end

  @doc """
  Returns a client accessible URL to the file.
  This uses the `:asset_host` defined in the `:arc` config.
  """
  @spec full_url(String.t()) :: String.t()
  def full_url(path) do
    path = String.replace(path, "priv/static/", "")
    host = Application.get_env(:arc, :asset_host, "")
    host <> path
  end

  def full_url({file, scope}, version) do
    path = url({file, scope}, version)
    full_url(path)
  end
end
