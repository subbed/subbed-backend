defmodule SubbedWeb.GraphqlContext do
  @moduledoc """
  This module serves as a struct that will be passed as the context property of
  the resolution argument to every GraphQL Resolver.

  The functions in this module are helper functions to easily mutate the struct.

  Please keep in mind that these functions should most likely take the
  GraphqlContext first and return a GraphqlContext as their result, so
  that we can easily chain function calls with the pipe operator.

  Something like:
  ```
  %GraphqlContext
  |> add_user(conn)
  |> add_foo(bar)
  ```
  """
  alias SubbedWeb.GraphqlContext
  alias Subbed.Account

  import Plug.Conn

  defstruct [:user]

  @type t() :: %GraphqlContext{}

  @doc """
  Takes a GraphqlContext and Plug Connection and looks up the user found in the
  session and adds them to the GraphqlContext. If the user doesn't exist, this
  is a no-op and simply returns the context.
  """
  @spec add_user(GraphqlContext.t(), Plug.Conn.t()) :: GraphqlContext.t()
  def add_user(context, conn) do
    with conn <- fetch_session(conn),
         user_id when not is_nil(user_id) <- get_session(conn, :user_id),
         user when not is_nil(user) <- Account.get_user(user_id) do
      Map.put(context, :user, user)
    else
      _ ->
        context
    end
  end
end
