defmodule SubbedWeb.Router do
  use SubbedWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  pipeline :graphql do
    plug(SubbedWeb.Plugs.Context)
  end

  scope "/", SubbedWeb do
    pipe_through(:browser)

    get("/", PageController, :index)
    get("/logout", AuthController, :logout)
  end

  scope "/auth", SubbedWeb do
    pipe_through(:browser)

    get("/:provider", AuthController, :request)
    get("/:provider/callback", AuthController, :callback)
  end

  scope "/api" do
    pipe_through(:graphql)

    forward("/", Absinthe.Plug, schema: SubbedWeb.Schema)
  end

  # Other scopes may use custom stacks.
  # scope "/api", SubbedWeb do
  #   pipe_through :api
  # end
end
