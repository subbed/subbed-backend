defmodule SubbedWeb.Resolvers.Account do
  def identity(_parent, _args, %{context: %{user: nil}}) do
    {:ok, nil}
  end

  def identity(_parent, _args, %{context: %{user: user}}) do
    {:ok, user}
  end
end
