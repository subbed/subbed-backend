defmodule SubbedWeb.Resolvers.Schedule do
  alias Subbed.{Content, Enums, Schedule, Repo}

  def list_platforms(_parent, _args, _resolution) do
    {:ok, Schedule.list_platforms()}
  end

  @type event_input :: %{
          required(:end_date) => Date.t(),
          required(:start_date) => Date.t(),
          required(:start_time) => Time.t(),
          required(:end_time) => Time.t(),
          required(:platform) => String.t(),
          required(:is_full_day_event) => boolean(),
          required(:is_recurring) => boolean(),
          # This is a String because the `:id` type on GraphQL is a String
          required(:creator_id) => String.t(),
          required(:medium_id) => String.t(),
          optional(:recurring_type) => Enums.RecurringType.t(),
          optional(:separation_count) => integer()
        }

  @type create_event_args :: %{required(:event) => event_input()}
  @spec create_event(any(), create_event_args(), any()) ::
          {:ok, Schedule.Event.t()} | {:error, Ecto.Changeset.t()}
  def create_event(_parent, args, %{context: %{user: user}}) do
    %{event: event_params} = args

    if event_params.is_recurring do
      %{separation_count: separation_count, recurring_type: recurring_type} = event_params

      event_params =
        event_params
        |> Map.delete(:separation_count)
        |> Map.delete(:recurring_type)
        |> Map.put(:owner_id, user.id)

      Schedule.create_recurring_event(
        struct(Schedule.Event.Params, event_params),
        separation_count,
        recurring_type
      )
    else
      args
      |> Map.get(:event)
      |> Map.put(:owner_id, user.id)
      |> Schedule.create_event()
    end
  end

  def list_creators(_parent, %{my_creators_filter: my_creators_filter}, %{context: %{user: user}}) do
    {:ok, Content.list_creators(my_creators_filter, user)}
  end

  def list_creators(_parent, _args, _resolution) do
    {:ok, Content.list_creators()}
  end

  def get_creator(_parent, %{id: id}, _resolution) do
    {:ok, Content.get_creator!(id)}
  end

  def get_creator(_parent, %{name: name}, _resolution) do
    {:ok, Content.get_creator_by_name!(name)}
  end

  @type list_event_args :: %{
          optional(:end_time) => Date.t(),
          optional(:start_time) => Date.t(),
          optional(:media_type) => Enums.MediaTypeEnum.t(),
          optional(:sort_by) => :start_date | :inserted_at,
          optional(:with_subscription) => boolean(),
          end_date: Date.t(),
          start_date: Time.t()
        }
  @spec list_events(map(), list_event_args(), map()) :: {:ok, list(map())}
  def list_events(_parent, args, %{context: %{user: user}}) do
    %{end_date: end_date, start_date: start_date} = args

    start_time = Map.get(args, :start_time)
    end_time = Map.get(args, :end_time)
    date_range = Map.get(args, :date_range)
    options = args |> Map.take([:media_type, :sort_by]) |> Map.to_list()

    options =
      if Map.get(args, :with_subscription, false) do
        Keyword.put(options, :user_subscription, user.id)
      else
        options
      end

    events = Schedule.list_events(start_date, end_date, start_time, end_time, date_range, options)
    {:ok, events}
  end

  @type create_subscription_args :: %{
          optional(:subscription_type) => Enums.SubscriptionTypeEnum.t(),
          optional(:medium_id) => integer(),
          creator_id: integer()
        }
  @spec create_subscription(map(), create_subscription_args(), map()) ::
          {:ok, Schedule.Subscription.t()} | {:error, String.t()}
  def create_subscription(_parent, _args, %{context: %{user: nil}}) do
    {:error, "Must be logged in to subscribe to creators"}
  end

  def create_subscription(_parent, args, %{context: %{user: user}}) do
    creator_id = Map.get(args, :creator_id)
    medium_id = Map.get(args, :medium_id)
    subscription_type = Map.get(args, :subscription_type)
    Schedule.create_subscription(user.id, creator_id, medium_id, subscription_type)
  end

  @type delete_subscription_args :: %{
          optional(:creator_id) => integer(),
          optional(:medium_id) => integer()
        }

  @spec delete_subscription(map(), delete_subscription_args(), map()) ::
          {:ok, Schedule.Subscription.t()} | {:error, String.t()}
  def delete_subscription(_parent, _args, %{context: %{user: nil}}) do
    {:error, "Must be logged in to unsubscribe from a creator"}
  end

  def delete_subscription(_parent, args, %{context: %{user: user}}) do
    creator_id = Map.get(args, :creator_id)
    medium_id = Map.get(args, :medium_id)

    entity =
      cond do
        not is_nil(medium_id) ->
          Repo.get(Content.Media, medium_id)

        not is_nil(creator_id) ->
          Repo.get(Content.Creator, creator_id)

        true ->
          nil
      end

    case entity do
      %Content.Media{} ->
        case Schedule.delete_subscription(user, entity) do
          {:ok, sub} -> {:ok, [sub]}
          {:error, error} -> {:error, error}
        end

      %Content.Creator{} ->
        {_, deleted_subscriptions} = Schedule.delete_subscriptions(user, entity)
        {:ok, deleted_subscriptions}

      nil ->
        {:error, "Cannot find something to unsubscribe from"}
    end
  end
end
