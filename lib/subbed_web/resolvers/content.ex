defmodule SubbedWeb.Resolvers.Content do
  alias Subbed.{Account, Content, Repo}
  import Ecto.Query

  @type add_maintainer_args :: %{user_id: integer(), creator_id: integer()}
  @spec add_maintainer(map(), add_maintainer_args(), map()) ::
          {:ok, User.t()} | {:error, String.t()}
  def add_maintainer(_parent, args, _resolution) do
    %{user_id: user_id, creator_id: creator_id} = args

    case Content.add_maintainer(creator_id, user_id) do
      {:ok, _} ->
        {:ok, Account.get_user(user_id)}

      {:error, changeset} ->
        {:error, changeset}
    end
  end

  def list_media(_parent, _args, _resolution) do
    query = from(medium in Content.Media, as: :medium)

    media =
      query
      |> Content.Media.load_subscription_count()
      |> Repo.all()

    {:ok, media}
  end

  def add_creator_avatar(_parent, _args, %{context: %{user: nil}}) do
    {:error, :unauthorized}
  end

  def add_creator_avatar(_parent, args, %{context: %{user: user}}) do
    %{creator_id: creator_id, avatar: avatar} = args

    params = %{avatar: avatar}

    with true <- Content.maintainer?(creator_id, user.id),
         creator when not is_nil(creator) <- Content.get_creator(creator_id),
         {:ok, creator} <- Content.update_creator(creator, params) do
      {:ok, creator}
    else
      false ->
        {:error, :unauthorized}

      {:error, changeset} ->
        {:error, changeset}
    end
  end
end
