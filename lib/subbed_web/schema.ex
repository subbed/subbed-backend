defmodule SubbedWeb.Schema do
  use Absinthe.Schema
  import_types(SubbedWeb.Schema.ContentTypes)
  import_types(Absinthe.Plug.Types)

  alias SubbedWeb.Resolvers
  alias Crudry.Middlewares.TranslateErrors
  alias Subbed.Schedule
  alias Subbed.Constants

  @desc "Range of date to fetch"
  enum :date_filter do
    value(:hour, description: "This hour")
    value(:day, description: "Today")
    value(:week, description: "This week")
    value(:month, description: "This month")
  end

  # Since we define the default value for the events using Timex
  # We have to make sure that tzdata is started at compile time.
  Application.ensure_all_started(:tzdata)

  def context(ctx) do
    loader =
      Dataloader.add_source(
        Dataloader.new(),
        Schedule,
        Schedule.data()
      )

    Map.put(ctx, :loader, loader)
  end

  def plugins do
    [Absinthe.Middleware.Dataloader] ++ Absinthe.Plugin.defaults()
  end

  query do
    @desc "Get all Platforms"
    field :platforms, list_of(:platform) do
      resolve(&Resolvers.Schedule.list_platforms/3)
    end

    @desc "Get all Creators"
    field :creators,
          :creator
          |> non_null()
          |> list_of()
          |> non_null() do
      arg(:default_platform, :string)
      arg(:my_creators_filter, :my_creators_filter_field)
      resolve(&Resolvers.Schedule.list_creators/3)
    end

    @desc "Get Creator"
    field :creator, :creator do
      arg(:id, :string)
      arg(:name, :string)
      resolve(&Resolvers.Schedule.get_creator/3)
    end

    @desc "Get user identity from session"
    field :identity, :user do
      resolve(&Resolvers.Account.identity/3)
    end

    @desc "Get all media"
    field :media, list_of(:medium) do
      resolve(&Resolvers.Content.list_media/3)
    end

    @desc "Get all events"
    field :events, list_of(:event) do
      arg(:date_range, :date_filter, default_value: nil)
      arg(:start_date, :date, default_value: Timex.to_date(Timex.now()))
      arg(:end_date, :date, default_value: default_event_end_date())
      arg(:start_time, :time, default_value: nil)
      arg(:end_time, :time, default_value: nil)
      arg(:media_type, :media_type, default_value: nil)
      arg(:sort_by, :event_sort_field, default_value: Constants.event_sort_fields().start_date)

      arg(:with_subscription, :boolean,
        default_value: false,
        description: "If true, returns only events for content this user is subscribed to."
      )

      resolve(&Resolvers.Schedule.list_events/3)
    end
  end

  ## Mutations

  mutation do
    @desc "Mutation to Create an Event"
    field :create_event, :event do
      arg(:event, :event_input)
      resolve(&Resolvers.Schedule.create_event/3)
    end

    @desc "Adds the user as a maintainer for the creator"
    field :add_maintainer, :user do
      arg(:user_id, non_null(:integer))
      arg(:creator_id, non_null(:integer))

      resolve(&Resolvers.Content.add_maintainer/3)
    end

    field :add_creator_avatar, :creator do
      arg(:creator_id, non_null(:id))
      arg(:avatar, non_null(:upload))

      resolve(&Resolvers.Content.add_creator_avatar/3)
    end

    field :create_subscription, :content_subscription do
      arg(:creator_id, non_null(:id))
      arg(:medium_id, :id)
      arg(:subscription_type, :subscription_type)

      resolve(&Resolvers.Schedule.create_subscription/3)
    end

    field :delete_subscription, list_of(:content_subscription) do
      arg(:creator_id, :id)
      arg(:medium_id, :id)

      resolve(&Resolvers.Schedule.delete_subscription/3)
    end
  end

  def middleware(middleware, _field, _object) do
    middleware ++ [TranslateErrors]
  end

  defp default_event_end_date do
    Timex.now()
    |> Timex.shift(weeks: 1)
    |> Timex.to_date()
  end
end
