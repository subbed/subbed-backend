defmodule SubbedWeb.Plugs.Context do
  @behaviour Plug

  alias Subbed.Account
  alias SubbedWeb.GraphqlContext
  import Plug.Conn

  def init(opts), do: opts

  @doc """
  Builds a `Context.t()` struct and adds it to the connection to be used in
  GraphQL Resolvers.
  """
  @spec call(Plug.Conn.t(), any()) :: Plug.Conn.t()
  def call(conn, _) do
    context = add_user(%{}, conn)
    Absinthe.Plug.put_options(conn, context: context)
  end

  @spec add_user(map(), Plug.Conn.t()) :: GraphqlContext.t()
  defp add_user(context, conn) do
    with conn <- fetch_session(conn),
         user_id when not is_nil(user_id) <- get_session(conn, :user_id),
         user when not is_nil(user) <- Account.get_user(user_id) do
      Map.put(context, :user, user)
    else
      _ ->
        Map.put(context, :user, nil)
    end
  end
end
