defmodule SubbedWeb.Schema.ContentTypes do
  use Absinthe.Schema.Notation
  import_types(Absinthe.Type.Custom)
  alias Subbed.{Constants, Content, Schedule}
  # I don't know why i need to use alias instead of import for the absinthe helpers
  alias Absinthe.Resolution.Helpers
  alias SubbedWeb.Uploaders

  object :platform do
    field(:id, non_null(:id))
    field(:name, non_null(:string))
    field(:url, non_null(:string))
  end

  object :content_subscription do
    field(:id, non_null(:id))
    field(:type, :string)
    field(:creator_id, non_null(:id))
    field(:medium_id, :id)
    field(:user, :user)

    field(:creator, :creator,
      resolve:
        Helpers.dataloader(Schedule, fn subscription, args, _ ->
          {:creator, Map.put(args, :creator_id, subscription.creator_id)}
        end)
    )

    field(:medium, :medium,
      resolve:
        Helpers.dataloader(Schedule, fn subscription, args, _ ->
          {:medium, Map.put(args, :medium_id, subscription.medium_id)}
        end)
    )
  end

  enum :subscription_type do
    value(:new_content, as: Constants.subscription_types().new_content)
    value(:all, as: Constants.subscription_types().all)
  end

  enum :my_creators_filter_field do
    value(:maintainer, as: Constants.my_creators_filter_fields().maintainer)
    value(:subscribed_to, as: Constants.my_creators_filter_fields().subscribed_to)
  end

  object :creator do
    field(:id, non_null(:id))
    field(:name, non_null(:string))
    field(:timezone, :string)
    field(:subscription_count, :integer)

    field :events, list_of(non_null(:event)) do
      arg(:start_date, :date, default_value: Timex.to_date(Timex.now()))

      arg(:end_date, :date, default_value: Timex.now() |> Timex.shift(weeks: 1) |> Timex.to_date())

      arg(:start_time, :time, default_value: nil)
      arg(:end_time, :time, default_value: nil)
      arg(:media_type, :media_type, default_value: nil)
      arg(:sort_by, :event_sort_field, default_value: Constants.event_sort_fields().start_date)

      resolve(fn creator, args, _ ->
        %{end_date: end_date, start_date: start_date} = args

        start_time = Map.get(args, :start_time)
        end_time = Map.get(args, :end_time)
        date_range = Map.get(args, :date_range)

        options =
          args
          |> Map.take([:media_type, :sort_by])
          |> Map.to_list()
          |> Keyword.put(:creator_id, creator.id)

        events =
          Schedule.list_events(start_date, end_date, start_time, end_time, date_range, options)

        {:ok, events}
      end)
    end

    field(:avatar, :string,
      resolve: fn creator, _, _ ->
        if not is_nil(creator.avatar) do
          {:ok, Uploaders.Avatar.full_url({creator.avatar.file_name, creator}, :original)}
        else
          {:ok, nil}
        end
      end
    )

    field(:avatar_thumbnail, :string,
      resolve: fn creator, _, _ ->
        if not is_nil(creator.avatar) do
          {:ok, Uploaders.Avatar.full_url({creator.avatar.file_name, creator}, :thumbnail)}
        else
          {:ok, nil}
        end
      end
    )
  end

  object :user do
    field(:id, non_null(:id))
    field(:user_id, non_null(:string))

    field(:subscriptions, list_of(non_null(:creator))) do
      resolve(fn user, _, _ ->
        {:ok, Content.list_creators(Constants.my_creators_filter_fields().subscribed_to, user)}
      end)
    end
  end

  enum :event_platform do
    value(:twitch, as: Constants.platforms().twitch)
    value(:youtube, as: Constants.platforms().youtube)
  end

  enum :recurring_type do
    value(:daily, as: Constants.recurring_types().daily)
    value(:weekly, as: Constants.recurring_types().weekly)
    value(:monthly, as: Constants.recurring_types().monthly)
    value(:yearly, as: Constants.recurring_types().yearly)
  end

  enum :media_type do
    value(:podcast, as: Constants.medium_types().podcast)
    value(:show, as: Constants.medium_types().show)
    value(:stream, as: Constants.medium_types().stream)
    value(:video, as: Constants.medium_types().video)
  end

  enum :event_sort_field do
    value(:start_date, as: Constants.event_sort_fields().start_date)
    value(:inserted_at, as: Constants.event_sort_fields().inserted_at)
  end

  input_object :event_input do
    field(:name, non_null(:string), default_value: "")
    field(:description, non_null(:string), default_value: "")
    field(:creator_id, non_null(:id))
    field(:medium_id, non_null(:id))
    field(:start_time, non_null(:time))
    field(:end_time, :time)
    field(:start_date, non_null(:date))

    field(:end_date, non_null(:date),
      description: "The end date for the event. If null, then this event never ends."
    )

    field(:platform, non_null(:event_platform))
    field(:is_full_day_event, non_null(:boolean), default_value: false)
    field(:is_recurring, non_null(:boolean), default_value: false)
    field(:recurring_type, :recurring_type, description: "How this event recurrs.")

    field(:separation_count, :integer,
      description:
        "The number of recurring_types between events. For example, if the recurring_type is DAILY and this is 1, this event will happen every day."
    )
  end

  object :medium do
    field(:id, non_null(:id))
    field(:name, non_null(:string))
    field(:creator_id, non_null(:id))
    field(:type, non_null(:string), description: "Type of event. Eg stream, video, podcast.")

    field(:subscription_count, non_null(:integer),
      description: "The number of users subscribed to this medium."
    )
  end

  object :event do
    field(:id, :id)
    field(:name, non_null(:string))
    field(:description, non_null(:string))
    field(:owner, :user)
    field(:start_time, non_null(:time))
    field(:end_time, non_null(:time))
    field(:start_date, non_null(:date))
    field(:end_date, :date)
    field(:platform, non_null(:event_platform))
    field(:is_full_day_event, non_null(:boolean))
    field(:is_recurring, non_null(:boolean))
    field(:inserted_at, non_null(:naive_datetime))

    field(:creator, :creator,
      resolve:
        Helpers.dataloader(Schedule, fn event, args, _ ->
          # TODO: Understand why this works? Apparently providing the creator_id corrects the relationship
          {:creator, Map.put(args, :creator_id, event.creator_id)}
        end)
    )

    field(:medium, :medium,
      resolve:
        Helpers.dataloader(Schedule, fn event, args, _ ->
          {:medium, Map.put(args, :medium_id, event.medium_id)}
        end)
    )
  end
end
