defmodule SubbedWeb.AuthController do
  use SubbedWeb, :controller
  plug(Ueberauth)

  alias Subbed.Account

  def callback(%{assigns: %{ueberauth_failure: _fails}} = conn, _params) do
    redirect(conn, to: "/")
  end

  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, _params) do
    client_url = Application.get_env(:subbed, SubbedWeb.Endpoint)[:client_url]

    email =
      auth
      |> Map.get(:info)
      |> Map.get(:email)

    user = Account.get_user_by_user_id(email)

    if not is_nil(user) do
      conn
      |> add_user_to_session(user)
      |> redirect(external: client_url)
    else
      case Account.create_user(email) do
        {:ok, user} ->
          conn
          |> add_user_to_session(user)
          |> redirect(external: client_url)

        {:error, _} ->
          conn
          |> put_flash(:error, "Failed to authenticate.")
          |> redirect(external: client_url)
      end
    end
  end

  def logout(conn, _params) do
    client_url = Application.get_env(:subbed, SubbedWeb.Endpoint)[:client_url]

    conn
    |> configure_session(drop: true)
    |> redirect(external: client_url)
  end

  @spec add_user_to_session(Plug.Conn.t(), Account.User.t()) :: Plug.Conn.t()
  defp add_user_to_session(conn, user) do
    conn
    |> put_session(:user_id, user.id)
    |> configure_session(renew: true)
  end
end
