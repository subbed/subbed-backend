defmodule SubbedWeb.PageController do
  use SubbedWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
